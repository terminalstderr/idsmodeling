%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Author: Ryan Leonard
%% Author: Atul Bohara
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\label{sec:idsModel}

In this section, we will discuss the steps that were taken to move from an IDS description to a SANs model that describes that IDS and reward variables that describe how we attain accuracy measurements. We will not discuss the accuracy results derived until the subsequent section (section \ref{sec:results}).

Before delving into details of how this modeling was performed, we will state the simplifying assumptions that were made in order to keep the produced state space finite.

\begin{assm}
Network delays and processing times are instantaneous -- this assumption is made because the concept of our reward rate structure requires that our decision engine and generation engine are perfectly synchronized. Though this may seem to be a pertinent assumption when considering measurements on an IDS, our model implicitly can not care about performance metrics, even though they might effect accuracy (e.g. packets dropped).
\end{assm}

\begin{assm}
Network scanning attackers query the various nodes of the destination subnet with uniform distribution, i.e. if there are three destinations, each attacker has one-third probability with each packet to access each destination respectively. This is a significant behavioral simplification of attackers, as such we would wish with future work to expand upon our attacker concepts to being more constrained or complicated.
\end{assm}

\begin{assm}
Non attacking source actors only access a \textit{fixed} subset of the destination subnet. As an example, in our first network we assume that \textit{Src1} can only access \textit{Dst1}.
\end{assm}

We implore when reading the remainder of this section to realize that the described models are acting on a packet stream. We represent the packet stream abstractly with a single token that iterates through the composed model $N$ times. Our SANs model thus has a very regular, and sequential ``decision'' process which reflects that of a probabilistic IDS.

\subsection{SANs Models}
We have two SANs models that we use to describe the probabilistic IDS. The first describes the process for generation of a packet stream. Our second model describes the logic of the IDS detection engine, as was described in section \ref{sec:ids}. In order to allow our packet generator to be modeled as a CTMC, we decided that we would use primarily instantaneous activities, along with one timed activity with exponential distribution $\gamma = 1$. We imagine this \textit{clock} activity as providing synchronous \textit{ticks} for both the generator and detector models. Thus, we imagine one packet arriving at every tick which instantaneously passes through both the generation and detection engines. Though this process is happening instantaneously, we imagine and describe the tokens of our SANs progressing through the composed model as a decision process. These thoughts become relevant when considering reward structures.

% Generator Description
\textbf{Generator:} Our generator has five essential places, which are required for the core logic of our model. The \textit{Arrival} place indicates that a packet should be generated. The \textit{GoodFlag} and \textit{BadFlag} places indicate whether the generated packet is from a scanner or is from a non-malicious source. The \textit{Source} place describes what source the packet generated has come from; we imagine this to be the source IP address provided in the header information of the packet. Similarly, the \textit{Destination} place represents the destination IP address of the packet. 

The generator conceptually has two decision points. First, the generator decides what source the packet is coming from. This decision is actually broken down into a decision as to whether the packet is good or bad, then deciding which particular source the packet came from. Second, once we know which source the packet has come from, we use that information to decide which destination the packet will be accessing. As mentioned in our assumptions, the set of possible destinations per source is completely hard coded in the generator.

% Detector Description
\textbf{Detector:} Our detector has five essential places. The \textit{Source} and \textit{Destination} places in the detector model are synonymous in meaning to the same named places in the generator model. The \textit{Good} and \textit{Bad} places indicate whether the packet packet is detected to be from a scanner or is from a non-malicious source; e.g. if there is a token in Bad, then the detector has determined that the packet is from a network scanner source. The \textit{Departure} place is used as an indicator that the packet has had detection mechanism run on it.

Besides these essential places, there are additional places that correspond to history of each source or destination. As we discussed in section \ref{sec:ids}, we need to keep store of what sources has accessed each destination, and which destinations each source has accessed (which can be represented in two-dimensions trivially). Each of these additional places stores an array that corresponds to this information. Each element of these arrays is either value 0, implying no access, or 1, implying access. For example, if \textit{Dst1\_Hist}'s representative array has the following values:\\
$[ 1,0,0,0,0,1 ]$
then we see that destination 1 has been accessed by only source 1 and source 6. This example makes it abrasively clear that our possible set of states is very limited, and will reach a steady state if we consider a minimal set of sources. We will venture more into this steady-state behavior in section \ref{sec:results}.

\begin{table}
    \begin{tabular}[t]{|l|l|}
        \hline     
        State Variable Name & Submodel Variables        \\ \hline
        Arrival     & Generator$\rightarrow$Arrival       $\equiv$  Detector$\rightarrow$Departure \\ \hline
        Destination & Generator$\rightarrow$Destination   $\equiv$  Detector$\rightarrow$Destination \\ \hline
        Source      &  Generator$\rightarrow$Source       $\equiv$  Detector$\rightarrow$Source \\ \hline
    \end{tabular}
    \label{tab:shared}
    \caption{Composition of Detection and Generator models via shared places.}
\end{table}

\textbf{Composition:} These two models are composed using a rep/join structure. The generator and detector SAN are joined, and share places as described by table \ref{tab:shared}.


Now that we have understood both of the models and how they are composed, we can perform an example packet run through of our composed model. We assume that we begin with Arrival state having a marking of 1. This prompts our generators clock activity to ``tick.'' Once the tick has been completed, the source, then destination will be probabilistically chosen. Based on what source is chosen, it will be indicated whether the packet is good or bad by marking one with the value 1, and one with the value 0. Now that the Source and Destination state have been populated, we switch to the detector SAN model. The detector updates its histories with the packet values, then makes a decision as to whether the current packet comes from a good or bad source. This decision is used to indicate whether the packet is detected good or bad similarly to in the generator model. Now, the Source and Destination state are nullified, and the Arrival state is once again populated.

\subsection{Reward Variables}
Our goal with reward variables is to peer into the differences in what was generated vs what was detected. We have used the ``good,'' and ``bad'' places to represent this concept from packet to packet. Now we simply need to create rate based reward variables that capture the variance of these values over time. We create four reward variables in total, one for each of the following, True Positive, True Negative, False Positive, and False Negative.

We will go into the particulars and describe one of these variables, but will allow the reader to extrapolate the other three. The False Positive variable contains a rate based reward structure, which either returns value 1 or 0. It makes this decision by seeing if the following query holds true:
\begin{verbatim}
if (Generator->GoodFlag == 1)
    if (Detector->Bad == 1)
        return 1;
\end{verbatim}
Explained in plain English, if the generator generates a non-malicious based packet, and the detector says that the packet in fact \textit{is} malicious, then we will increment a count to this reward variable.
