#ifndef IDSRJ_H_
#define IDSRJ_H_
#include <math.h>
#include "Cpp/Composer/Join.h"
#include "Cpp/Composer/AllStateVariableTypes.h"
#include "Atomic/Generator/GeneratorSAN.h"
#include "Atomic/Detector/DetectorSAN.h"

//State variable headers
#include "Cpp/BaseClasses/SAN/Place.h"

class IDSRJ: public Join {
 public:
  GeneratorSAN * Generator;
  DetectorSAN * Detector;
  Place * Arrival;
  Place * Destination;
  Place * Source;

  IDSRJ();
  ~IDSRJ();
};

#endif
