

// This C++ file was created by SanEditor

#include "Atomic/Generator/GeneratorSAN.h"

#include <stdlib.h>
#include <iostream>

#include <math.h>


/*****************************************************************
                         GeneratorSAN Constructor             
******************************************************************/


GeneratorSAN::GeneratorSAN(){


  SourceTypeGroup.initialize(2, "SourceTypeGroup");
  SourceTypeGroup.appendGroup((BaseGroupClass*) &SourceType_case1);
  SourceTypeGroup.appendGroup((BaseGroupClass*) &SourceType_case2);

  BadPacketSourceGroup.initialize(2, "BadPacketSourceGroup");
  BadPacketSourceGroup.appendGroup((BaseGroupClass*) &BadPacketSource_case1);
  BadPacketSourceGroup.appendGroup((BaseGroupClass*) &BadPacketSource_case2);

  GoodPacketSourceGroup.initialize(4, "GoodPacketSourceGroup");
  GoodPacketSourceGroup.appendGroup((BaseGroupClass*) &GoodPacketSource_case1);
  GoodPacketSourceGroup.appendGroup((BaseGroupClass*) &GoodPacketSource_case2);
  GoodPacketSourceGroup.appendGroup((BaseGroupClass*) &GoodPacketSource_case3);
  GoodPacketSourceGroup.appendGroup((BaseGroupClass*) &GoodPacketSource_case4);

  Access5DstGroup.initialize(3, "Access5DstGroup");
  Access5DstGroup.appendGroup((BaseGroupClass*) &Access5Dst_case1);
  Access5DstGroup.appendGroup((BaseGroupClass*) &Access5Dst_case2);
  Access5DstGroup.appendGroup((BaseGroupClass*) &Access5Dst_case3);

  Access6DstGroup.initialize(3, "Access6DstGroup");
  Access6DstGroup.appendGroup((BaseGroupClass*) &Access6Dst_case1);
  Access6DstGroup.appendGroup((BaseGroupClass*) &Access6Dst_case2);
  Access6DstGroup.appendGroup((BaseGroupClass*) &Access6Dst_case3);

  Activity* InitialActionList[19]={
    &SourceType_case1, //0
    &SourceType_case2, //1
    &BadPacketSource_case1, //2
    &BadPacketSource_case2, //3
    &GoodPacketSource_case1, //4
    &GoodPacketSource_case2, //5
    &GoodPacketSource_case3, //6
    &GoodPacketSource_case4, //7
    &Access1Dst, //8
    &Access2Dst, //9
    &Access3Dst, //10
    &Access4Dst, //11
    &Access5Dst_case1, //12
    &Access5Dst_case2, //13
    &Access5Dst_case3, //14
    &Access6Dst_case1, //15
    &Access6Dst_case2, //16
    &Access6Dst_case3, //17
    &Clock  // 18
  };

  BaseGroupClass* InitialGroupList[10]={
    (BaseGroupClass*) &(Clock), 
    (BaseGroupClass*) &(SourceTypeGroup), 
    (BaseGroupClass*) &(BadPacketSourceGroup), 
    (BaseGroupClass*) &(GoodPacketSourceGroup), 
    (BaseGroupClass*) &(Access1Dst), 
    (BaseGroupClass*) &(Access2Dst), 
    (BaseGroupClass*) &(Access3Dst), 
    (BaseGroupClass*) &(Access4Dst), 
    (BaseGroupClass*) &(Access5DstGroup), 
    (BaseGroupClass*) &(Access6DstGroup)
  };

  Arrival = new Place("Arrival" ,1);
  GoodPacket = new Place("GoodPacket" ,0);
  BadPacket = new Place("BadPacket" ,0);
  GoodFlag = new Place("GoodFlag" ,0);
  BadFlag = new Place("BadFlag" ,0);
  Packet = new Place("Packet" ,0);
  Access1 = new Place("Access1" ,0);
  Access2 = new Place("Access2" ,0);
  Access3 = new Place("Access3" ,0);
  Access4 = new Place("Access4" ,0);
  Access5 = new Place("Access5" ,0);
  Access6 = new Place("Access6" ,0);
  short temp_Sourceshort = 0;
  Source = new ExtendedPlace<short>("Source",temp_Sourceshort);
  short temp_Destinationshort = 0;
  Destination = new ExtendedPlace<short>("Destination",temp_Destinationshort);
  BaseStateVariableClass* InitialPlaces[14]={
    Arrival,  // 0
    GoodPacket,  // 1
    BadPacket,  // 2
    GoodFlag,  // 3
    BadFlag,  // 4
    Packet,  // 5
    Access1,  // 6
    Access2,  // 7
    Access3,  // 8
    Access4,  // 9
    Access5,  // 10
    Access6,  // 11
    Source,  // 12
    Destination   // 13
  };
  BaseStateVariableClass* InitialROPlaces[0]={
  };
  initializeSANModelNow("Generator", 14, InitialPlaces, 
                        0, InitialROPlaces, 
                        19, InitialActionList, 10, InitialGroupList);


  assignPlacesToActivitiesInst();
  assignPlacesToActivitiesTimed();

  int AffectArcs[48][2]={ 
    {5,0}, {3,0}, {4,0}, {1,0}, {5,1}, {4,1}, {3,1}, {2,1}, {2,2}, 
    {12,2}, {10,2}, {2,3}, {12,3}, {11,3}, {1,4}, {12,4}, {6,4}, 
    {1,5}, {12,5}, {7,5}, {1,6}, {12,6}, {8,6}, {1,7}, {12,7}, 
    {9,7}, {6,8}, {13,8}, {7,9}, {13,9}, {8,10}, {13,10}, {9,11}, 
    {13,11}, {10,12}, {13,12}, {10,13}, {13,13}, {10,14}, {13,14}, 
    {11,15}, {13,15}, {11,16}, {13,16}, {11,17}, {13,17}, {0,18}, 
    {5,18}
  };
  for(int n=0;n<48;n++) {
    AddAffectArc(InitialPlaces[AffectArcs[n][0]],
                 InitialActionList[AffectArcs[n][1]]);
  }
  int EnableArcs[19][2]={ 
    {5,0}, {5,1}, {2,2}, {2,3}, {1,4}, {1,5}, {1,6}, {1,7}, {6,8}, 
    {7,9}, {8,10}, {9,11}, {10,12}, {10,13}, {10,14}, {11,15}, 
    {11,16}, {11,17}, {0,18}
  };
  for(int n=0;n<19;n++) {
    AddEnableArc(InitialPlaces[EnableArcs[n][0]],
                 InitialActionList[EnableArcs[n][1]]);
  }

  for(int n=0;n<19;n++) {
    InitialActionList[n]->LinkVariables();
  }
  CustomInitialization();

}

void GeneratorSAN::CustomInitialization() {

}
GeneratorSAN::~GeneratorSAN(){
  for (int i = 0; i < NumStateVariables-NumReadOnlyPlaces; i++)
    delete LocalStateVariables[i];
};

void GeneratorSAN::assignPlacesToActivitiesInst(){
  SourceType_case1.Packet = (Place*) LocalStateVariables[5];
  SourceType_case1.GoodFlag = (Place*) LocalStateVariables[3];
  SourceType_case1.BadFlag = (Place*) LocalStateVariables[4];
  SourceType_case1.GoodPacket = (Place*) LocalStateVariables[1];
  SourceType_case2.Packet = (Place*) LocalStateVariables[5];
  SourceType_case2.BadFlag = (Place*) LocalStateVariables[4];
  SourceType_case2.GoodFlag = (Place*) LocalStateVariables[3];
  SourceType_case2.BadPacket = (Place*) LocalStateVariables[2];
  BadPacketSource_case1.BadPacket = (Place*) LocalStateVariables[2];
  BadPacketSource_case1.Source = (ExtendedPlace<short>*) LocalStateVariables[12];
  BadPacketSource_case1.Access5 = (Place*) LocalStateVariables[10];
  BadPacketSource_case2.BadPacket = (Place*) LocalStateVariables[2];
  BadPacketSource_case2.Source = (ExtendedPlace<short>*) LocalStateVariables[12];
  BadPacketSource_case2.Access6 = (Place*) LocalStateVariables[11];
  GoodPacketSource_case1.GoodPacket = (Place*) LocalStateVariables[1];
  GoodPacketSource_case1.Source = (ExtendedPlace<short>*) LocalStateVariables[12];
  GoodPacketSource_case1.Access1 = (Place*) LocalStateVariables[6];
  GoodPacketSource_case2.GoodPacket = (Place*) LocalStateVariables[1];
  GoodPacketSource_case2.Source = (ExtendedPlace<short>*) LocalStateVariables[12];
  GoodPacketSource_case2.Access2 = (Place*) LocalStateVariables[7];
  GoodPacketSource_case3.GoodPacket = (Place*) LocalStateVariables[1];
  GoodPacketSource_case3.Source = (ExtendedPlace<short>*) LocalStateVariables[12];
  GoodPacketSource_case3.Access3 = (Place*) LocalStateVariables[8];
  GoodPacketSource_case4.GoodPacket = (Place*) LocalStateVariables[1];
  GoodPacketSource_case4.Source = (ExtendedPlace<short>*) LocalStateVariables[12];
  GoodPacketSource_case4.Access4 = (Place*) LocalStateVariables[9];
  Access1Dst.Access1 = (Place*) LocalStateVariables[6];
  Access1Dst.Destination = (ExtendedPlace<short>*) LocalStateVariables[13];
  Access2Dst.Access2 = (Place*) LocalStateVariables[7];
  Access2Dst.Destination = (ExtendedPlace<short>*) LocalStateVariables[13];
  Access3Dst.Access3 = (Place*) LocalStateVariables[8];
  Access3Dst.Destination = (ExtendedPlace<short>*) LocalStateVariables[13];
  Access4Dst.Access4 = (Place*) LocalStateVariables[9];
  Access4Dst.Destination = (ExtendedPlace<short>*) LocalStateVariables[13];
  Access5Dst_case1.Access5 = (Place*) LocalStateVariables[10];
  Access5Dst_case1.Destination = (ExtendedPlace<short>*) LocalStateVariables[13];
  Access5Dst_case2.Access5 = (Place*) LocalStateVariables[10];
  Access5Dst_case2.Destination = (ExtendedPlace<short>*) LocalStateVariables[13];
  Access5Dst_case3.Access5 = (Place*) LocalStateVariables[10];
  Access5Dst_case3.Destination = (ExtendedPlace<short>*) LocalStateVariables[13];
  Access6Dst_case1.Access6 = (Place*) LocalStateVariables[11];
  Access6Dst_case1.Destination = (ExtendedPlace<short>*) LocalStateVariables[13];
  Access6Dst_case2.Access6 = (Place*) LocalStateVariables[11];
  Access6Dst_case2.Destination = (ExtendedPlace<short>*) LocalStateVariables[13];
  Access6Dst_case3.Access6 = (Place*) LocalStateVariables[11];
  Access6Dst_case3.Destination = (ExtendedPlace<short>*) LocalStateVariables[13];
}
void GeneratorSAN::assignPlacesToActivitiesTimed(){
  Clock.Arrival = (Place*) LocalStateVariables[0];
  Clock.Packet = (Place*) LocalStateVariables[5];
}
/*****************************************************************/
/*                  Activity Method Definitions                  */
/*****************************************************************/

/*======================SourceTypeActivity_case1========================*/


GeneratorSAN::SourceTypeActivity_case1::SourceTypeActivity_case1(){
  ActivityInitialize("SourceType_case1",1,Instantaneous , RaceEnabled, 4,1, false);
}

void GeneratorSAN::SourceTypeActivity_case1::LinkVariables(){
  Packet->Register(&Packet_Mobius_Mark);
  GoodFlag->Register(&GoodFlag_Mobius_Mark);
  BadFlag->Register(&BadFlag_Mobius_Mark);
  GoodPacket->Register(&GoodPacket_Mobius_Mark);
}

bool GeneratorSAN::SourceTypeActivity_case1::Enabled(){
  OldEnabled=NewEnabled;
  NewEnabled=(((*(Packet_Mobius_Mark)) >=1));
  return NewEnabled;
}

double GeneratorSAN::SourceTypeActivity_case1::Weight(){ 
  return (s1+s2+s3+s4)/(s1+s2+s3+s4+s5+s6);
}

bool GeneratorSAN::SourceTypeActivity_case1::ReactivationPredicate(){ 
  return false;
}

bool GeneratorSAN::SourceTypeActivity_case1::ReactivationFunction(){ 
  return false;
}

double GeneratorSAN::SourceTypeActivity_case1::SampleDistribution(){
  return 0;
}

double* GeneratorSAN::SourceTypeActivity_case1::ReturnDistributionParameters(){
    return NULL;
}

int GeneratorSAN::SourceTypeActivity_case1::Rank(){
  return 1;
}

BaseActionClass* GeneratorSAN::SourceTypeActivity_case1::Fire(){
  (*(Packet_Mobius_Mark))--;
  // Set Flag to indicate source type
GoodFlag->Mark()=1;
BadFlag->Mark()=0;

// Pass forward source type information
GoodPacket->Mark() = 1;
  return this;
}

/*======================SourceTypeActivity_case2========================*/


GeneratorSAN::SourceTypeActivity_case2::SourceTypeActivity_case2(){
  ActivityInitialize("SourceType_case2",1,Instantaneous , RaceEnabled, 4,1, false);
}

void GeneratorSAN::SourceTypeActivity_case2::LinkVariables(){
  Packet->Register(&Packet_Mobius_Mark);
  BadFlag->Register(&BadFlag_Mobius_Mark);
  GoodFlag->Register(&GoodFlag_Mobius_Mark);
  BadPacket->Register(&BadPacket_Mobius_Mark);
}

bool GeneratorSAN::SourceTypeActivity_case2::Enabled(){
  OldEnabled=NewEnabled;
  NewEnabled=(((*(Packet_Mobius_Mark)) >=1));
  return NewEnabled;
}

double GeneratorSAN::SourceTypeActivity_case2::Weight(){ 
  return (s5+s6)/(s1+s2+s3+s4+s5+s6);
}

bool GeneratorSAN::SourceTypeActivity_case2::ReactivationPredicate(){ 
  return false;
}

bool GeneratorSAN::SourceTypeActivity_case2::ReactivationFunction(){ 
  return false;
}

double GeneratorSAN::SourceTypeActivity_case2::SampleDistribution(){
  return 0;
}

double* GeneratorSAN::SourceTypeActivity_case2::ReturnDistributionParameters(){
    return NULL;
}

int GeneratorSAN::SourceTypeActivity_case2::Rank(){
  return 1;
}

BaseActionClass* GeneratorSAN::SourceTypeActivity_case2::Fire(){
  (*(Packet_Mobius_Mark))--;
  // Set flag to indicate source type
BadFlag->Mark()=1;
GoodFlag->Mark()=0;

// Pass forward source type information
BadPacket->Mark() = 1;
  return this;
}

/*======================BadPacketSourceActivity_case1========================*/


GeneratorSAN::BadPacketSourceActivity_case1::BadPacketSourceActivity_case1(){
  ActivityInitialize("BadPacketSource_case1",2,Instantaneous , RaceEnabled, 3,1, false);
}

void GeneratorSAN::BadPacketSourceActivity_case1::LinkVariables(){
  BadPacket->Register(&BadPacket_Mobius_Mark);

  Access5->Register(&Access5_Mobius_Mark);
}

bool GeneratorSAN::BadPacketSourceActivity_case1::Enabled(){
  OldEnabled=NewEnabled;
  NewEnabled=(((*(BadPacket_Mobius_Mark)) >=1));
  return NewEnabled;
}

double GeneratorSAN::BadPacketSourceActivity_case1::Weight(){ 
  return s5/(s5+s6);
}

bool GeneratorSAN::BadPacketSourceActivity_case1::ReactivationPredicate(){ 
  return false;
}

bool GeneratorSAN::BadPacketSourceActivity_case1::ReactivationFunction(){ 
  return false;
}

double GeneratorSAN::BadPacketSourceActivity_case1::SampleDistribution(){
  return 0;
}

double* GeneratorSAN::BadPacketSourceActivity_case1::ReturnDistributionParameters(){
    return NULL;
}

int GeneratorSAN::BadPacketSourceActivity_case1::Rank(){
  return 1;
}

BaseActionClass* GeneratorSAN::BadPacketSourceActivity_case1::Fire(){
  (*(BadPacket_Mobius_Mark))--;
  Source->Mark()=5;
Access5->Mark()=1;
  return this;
}

/*======================BadPacketSourceActivity_case2========================*/


GeneratorSAN::BadPacketSourceActivity_case2::BadPacketSourceActivity_case2(){
  ActivityInitialize("BadPacketSource_case2",2,Instantaneous , RaceEnabled, 3,1, false);
}

void GeneratorSAN::BadPacketSourceActivity_case2::LinkVariables(){
  BadPacket->Register(&BadPacket_Mobius_Mark);

  Access6->Register(&Access6_Mobius_Mark);
}

bool GeneratorSAN::BadPacketSourceActivity_case2::Enabled(){
  OldEnabled=NewEnabled;
  NewEnabled=(((*(BadPacket_Mobius_Mark)) >=1));
  return NewEnabled;
}

double GeneratorSAN::BadPacketSourceActivity_case2::Weight(){ 
  return s6/(s5+s6);
}

bool GeneratorSAN::BadPacketSourceActivity_case2::ReactivationPredicate(){ 
  return false;
}

bool GeneratorSAN::BadPacketSourceActivity_case2::ReactivationFunction(){ 
  return false;
}

double GeneratorSAN::BadPacketSourceActivity_case2::SampleDistribution(){
  return 0;
}

double* GeneratorSAN::BadPacketSourceActivity_case2::ReturnDistributionParameters(){
    return NULL;
}

int GeneratorSAN::BadPacketSourceActivity_case2::Rank(){
  return 1;
}

BaseActionClass* GeneratorSAN::BadPacketSourceActivity_case2::Fire(){
  (*(BadPacket_Mobius_Mark))--;
  Source->Mark()=6;
Access6->Mark()=1;
  return this;
}

/*======================GoodPacketSourceActivity_case1========================*/


GeneratorSAN::GoodPacketSourceActivity_case1::GoodPacketSourceActivity_case1(){
  ActivityInitialize("GoodPacketSource_case1",3,Instantaneous , RaceEnabled, 3,1, false);
}

void GeneratorSAN::GoodPacketSourceActivity_case1::LinkVariables(){
  GoodPacket->Register(&GoodPacket_Mobius_Mark);

  Access1->Register(&Access1_Mobius_Mark);
}

bool GeneratorSAN::GoodPacketSourceActivity_case1::Enabled(){
  OldEnabled=NewEnabled;
  NewEnabled=(((*(GoodPacket_Mobius_Mark)) >=1));
  return NewEnabled;
}

double GeneratorSAN::GoodPacketSourceActivity_case1::Weight(){ 
  return s1/(s1+s2+s3+s4);
}

bool GeneratorSAN::GoodPacketSourceActivity_case1::ReactivationPredicate(){ 
  return false;
}

bool GeneratorSAN::GoodPacketSourceActivity_case1::ReactivationFunction(){ 
  return false;
}

double GeneratorSAN::GoodPacketSourceActivity_case1::SampleDistribution(){
  return 0;
}

double* GeneratorSAN::GoodPacketSourceActivity_case1::ReturnDistributionParameters(){
    return NULL;
}

int GeneratorSAN::GoodPacketSourceActivity_case1::Rank(){
  return 1;
}

BaseActionClass* GeneratorSAN::GoodPacketSourceActivity_case1::Fire(){
  (*(GoodPacket_Mobius_Mark))--;
  Source->Mark()=1;
Access1->Mark()=1;
  return this;
}

/*======================GoodPacketSourceActivity_case2========================*/


GeneratorSAN::GoodPacketSourceActivity_case2::GoodPacketSourceActivity_case2(){
  ActivityInitialize("GoodPacketSource_case2",3,Instantaneous , RaceEnabled, 3,1, false);
}

void GeneratorSAN::GoodPacketSourceActivity_case2::LinkVariables(){
  GoodPacket->Register(&GoodPacket_Mobius_Mark);

  Access2->Register(&Access2_Mobius_Mark);
}

bool GeneratorSAN::GoodPacketSourceActivity_case2::Enabled(){
  OldEnabled=NewEnabled;
  NewEnabled=(((*(GoodPacket_Mobius_Mark)) >=1));
  return NewEnabled;
}

double GeneratorSAN::GoodPacketSourceActivity_case2::Weight(){ 
  return s2/(s1+s2+s3+s4);
}

bool GeneratorSAN::GoodPacketSourceActivity_case2::ReactivationPredicate(){ 
  return false;
}

bool GeneratorSAN::GoodPacketSourceActivity_case2::ReactivationFunction(){ 
  return false;
}

double GeneratorSAN::GoodPacketSourceActivity_case2::SampleDistribution(){
  return 0;
}

double* GeneratorSAN::GoodPacketSourceActivity_case2::ReturnDistributionParameters(){
    return NULL;
}

int GeneratorSAN::GoodPacketSourceActivity_case2::Rank(){
  return 1;
}

BaseActionClass* GeneratorSAN::GoodPacketSourceActivity_case2::Fire(){
  (*(GoodPacket_Mobius_Mark))--;
  Source->Mark()=2;
Access2->Mark()=1;
  return this;
}

/*======================GoodPacketSourceActivity_case3========================*/


GeneratorSAN::GoodPacketSourceActivity_case3::GoodPacketSourceActivity_case3(){
  ActivityInitialize("GoodPacketSource_case3",3,Instantaneous , RaceEnabled, 3,1, false);
}

void GeneratorSAN::GoodPacketSourceActivity_case3::LinkVariables(){
  GoodPacket->Register(&GoodPacket_Mobius_Mark);

  Access3->Register(&Access3_Mobius_Mark);
}

bool GeneratorSAN::GoodPacketSourceActivity_case3::Enabled(){
  OldEnabled=NewEnabled;
  NewEnabled=(((*(GoodPacket_Mobius_Mark)) >=1));
  return NewEnabled;
}

double GeneratorSAN::GoodPacketSourceActivity_case3::Weight(){ 
  return s3/(s1+s2+s3+s4);
}

bool GeneratorSAN::GoodPacketSourceActivity_case3::ReactivationPredicate(){ 
  return false;
}

bool GeneratorSAN::GoodPacketSourceActivity_case3::ReactivationFunction(){ 
  return false;
}

double GeneratorSAN::GoodPacketSourceActivity_case3::SampleDistribution(){
  return 0;
}

double* GeneratorSAN::GoodPacketSourceActivity_case3::ReturnDistributionParameters(){
    return NULL;
}

int GeneratorSAN::GoodPacketSourceActivity_case3::Rank(){
  return 1;
}

BaseActionClass* GeneratorSAN::GoodPacketSourceActivity_case3::Fire(){
  (*(GoodPacket_Mobius_Mark))--;
  Source->Mark()=3;
Access3->Mark()=1;
  return this;
}

/*======================GoodPacketSourceActivity_case4========================*/


GeneratorSAN::GoodPacketSourceActivity_case4::GoodPacketSourceActivity_case4(){
  ActivityInitialize("GoodPacketSource_case4",3,Instantaneous , RaceEnabled, 3,1, false);
}

void GeneratorSAN::GoodPacketSourceActivity_case4::LinkVariables(){
  GoodPacket->Register(&GoodPacket_Mobius_Mark);

  Access4->Register(&Access4_Mobius_Mark);
}

bool GeneratorSAN::GoodPacketSourceActivity_case4::Enabled(){
  OldEnabled=NewEnabled;
  NewEnabled=(((*(GoodPacket_Mobius_Mark)) >=1));
  return NewEnabled;
}

double GeneratorSAN::GoodPacketSourceActivity_case4::Weight(){ 
  return s4/(s1+s2+s3+s4);
}

bool GeneratorSAN::GoodPacketSourceActivity_case4::ReactivationPredicate(){ 
  return false;
}

bool GeneratorSAN::GoodPacketSourceActivity_case4::ReactivationFunction(){ 
  return false;
}

double GeneratorSAN::GoodPacketSourceActivity_case4::SampleDistribution(){
  return 0;
}

double* GeneratorSAN::GoodPacketSourceActivity_case4::ReturnDistributionParameters(){
    return NULL;
}

int GeneratorSAN::GoodPacketSourceActivity_case4::Rank(){
  return 1;
}

BaseActionClass* GeneratorSAN::GoodPacketSourceActivity_case4::Fire(){
  (*(GoodPacket_Mobius_Mark))--;
  Source->Mark()=4;
Access4->Mark()=1;
  return this;
}

/*======================Access1DstActivity========================*/


GeneratorSAN::Access1DstActivity::Access1DstActivity(){
  ActivityInitialize("Access1Dst",4,Instantaneous , RaceEnabled, 2,1, false);
}

void GeneratorSAN::Access1DstActivity::LinkVariables(){
  Access1->Register(&Access1_Mobius_Mark);

}

bool GeneratorSAN::Access1DstActivity::Enabled(){
  OldEnabled=NewEnabled;
  NewEnabled=(((*(Access1_Mobius_Mark)) >=1));
  return NewEnabled;
}

double GeneratorSAN::Access1DstActivity::Weight(){ 
  return 1;
}

bool GeneratorSAN::Access1DstActivity::ReactivationPredicate(){ 
  return false;
}

bool GeneratorSAN::Access1DstActivity::ReactivationFunction(){ 
  return false;
}

double GeneratorSAN::Access1DstActivity::SampleDistribution(){
  return 0;
}

double* GeneratorSAN::Access1DstActivity::ReturnDistributionParameters(){
    return NULL;
}

int GeneratorSAN::Access1DstActivity::Rank(){
  return 1;
}

BaseActionClass* GeneratorSAN::Access1DstActivity::Fire(){
  (*(Access1_Mobius_Mark))--;
  Destination->Mark()=1;
  return this;
}

/*======================Access2DstActivity========================*/


GeneratorSAN::Access2DstActivity::Access2DstActivity(){
  ActivityInitialize("Access2Dst",5,Instantaneous , RaceEnabled, 2,1, false);
}

void GeneratorSAN::Access2DstActivity::LinkVariables(){
  Access2->Register(&Access2_Mobius_Mark);

}

bool GeneratorSAN::Access2DstActivity::Enabled(){
  OldEnabled=NewEnabled;
  NewEnabled=(((*(Access2_Mobius_Mark)) >=1));
  return NewEnabled;
}

double GeneratorSAN::Access2DstActivity::Weight(){ 
  return 1;
}

bool GeneratorSAN::Access2DstActivity::ReactivationPredicate(){ 
  return false;
}

bool GeneratorSAN::Access2DstActivity::ReactivationFunction(){ 
  return false;
}

double GeneratorSAN::Access2DstActivity::SampleDistribution(){
  return 0;
}

double* GeneratorSAN::Access2DstActivity::ReturnDistributionParameters(){
    return NULL;
}

int GeneratorSAN::Access2DstActivity::Rank(){
  return 1;
}

BaseActionClass* GeneratorSAN::Access2DstActivity::Fire(){
  (*(Access2_Mobius_Mark))--;
  Destination->Mark()=1;
  return this;
}

/*======================Access3DstActivity========================*/


GeneratorSAN::Access3DstActivity::Access3DstActivity(){
  ActivityInitialize("Access3Dst",6,Instantaneous , RaceEnabled, 2,1, false);
}

void GeneratorSAN::Access3DstActivity::LinkVariables(){
  Access3->Register(&Access3_Mobius_Mark);

}

bool GeneratorSAN::Access3DstActivity::Enabled(){
  OldEnabled=NewEnabled;
  NewEnabled=(((*(Access3_Mobius_Mark)) >=1));
  return NewEnabled;
}

double GeneratorSAN::Access3DstActivity::Weight(){ 
  return 1;
}

bool GeneratorSAN::Access3DstActivity::ReactivationPredicate(){ 
  return false;
}

bool GeneratorSAN::Access3DstActivity::ReactivationFunction(){ 
  return false;
}

double GeneratorSAN::Access3DstActivity::SampleDistribution(){
  return 0;
}

double* GeneratorSAN::Access3DstActivity::ReturnDistributionParameters(){
    return NULL;
}

int GeneratorSAN::Access3DstActivity::Rank(){
  return 1;
}

BaseActionClass* GeneratorSAN::Access3DstActivity::Fire(){
  (*(Access3_Mobius_Mark))--;
  Destination->Mark()=3;
  return this;
}

/*======================Access4DstActivity========================*/


GeneratorSAN::Access4DstActivity::Access4DstActivity(){
  ActivityInitialize("Access4Dst",7,Instantaneous , RaceEnabled, 2,1, false);
}

void GeneratorSAN::Access4DstActivity::LinkVariables(){
  Access4->Register(&Access4_Mobius_Mark);

}

bool GeneratorSAN::Access4DstActivity::Enabled(){
  OldEnabled=NewEnabled;
  NewEnabled=(((*(Access4_Mobius_Mark)) >=1));
  return NewEnabled;
}

double GeneratorSAN::Access4DstActivity::Weight(){ 
  return 1;
}

bool GeneratorSAN::Access4DstActivity::ReactivationPredicate(){ 
  return false;
}

bool GeneratorSAN::Access4DstActivity::ReactivationFunction(){ 
  return false;
}

double GeneratorSAN::Access4DstActivity::SampleDistribution(){
  return 0;
}

double* GeneratorSAN::Access4DstActivity::ReturnDistributionParameters(){
    return NULL;
}

int GeneratorSAN::Access4DstActivity::Rank(){
  return 1;
}

BaseActionClass* GeneratorSAN::Access4DstActivity::Fire(){
  (*(Access4_Mobius_Mark))--;
  Destination->Mark()=2;
  return this;
}

/*======================Access5DstActivity_case1========================*/


GeneratorSAN::Access5DstActivity_case1::Access5DstActivity_case1(){
  ActivityInitialize("Access5Dst_case1",8,Instantaneous , RaceEnabled, 2,1, false);
}

void GeneratorSAN::Access5DstActivity_case1::LinkVariables(){
  Access5->Register(&Access5_Mobius_Mark);

}

bool GeneratorSAN::Access5DstActivity_case1::Enabled(){
  OldEnabled=NewEnabled;
  NewEnabled=(((*(Access5_Mobius_Mark)) >=1));
  return NewEnabled;
}

double GeneratorSAN::Access5DstActivity_case1::Weight(){ 
  return d1/(d1+d2+d3);
}

bool GeneratorSAN::Access5DstActivity_case1::ReactivationPredicate(){ 
  return false;
}

bool GeneratorSAN::Access5DstActivity_case1::ReactivationFunction(){ 
  return false;
}

double GeneratorSAN::Access5DstActivity_case1::SampleDistribution(){
  return 0;
}

double* GeneratorSAN::Access5DstActivity_case1::ReturnDistributionParameters(){
    return NULL;
}

int GeneratorSAN::Access5DstActivity_case1::Rank(){
  return 1;
}

BaseActionClass* GeneratorSAN::Access5DstActivity_case1::Fire(){
  (*(Access5_Mobius_Mark))--;
  Destination->Mark()=1;
  return this;
}

/*======================Access5DstActivity_case2========================*/


GeneratorSAN::Access5DstActivity_case2::Access5DstActivity_case2(){
  ActivityInitialize("Access5Dst_case2",8,Instantaneous , RaceEnabled, 2,1, false);
}

void GeneratorSAN::Access5DstActivity_case2::LinkVariables(){
  Access5->Register(&Access5_Mobius_Mark);

}

bool GeneratorSAN::Access5DstActivity_case2::Enabled(){
  OldEnabled=NewEnabled;
  NewEnabled=(((*(Access5_Mobius_Mark)) >=1));
  return NewEnabled;
}

double GeneratorSAN::Access5DstActivity_case2::Weight(){ 
  return d2/(d1+d2+d3);
}

bool GeneratorSAN::Access5DstActivity_case2::ReactivationPredicate(){ 
  return false;
}

bool GeneratorSAN::Access5DstActivity_case2::ReactivationFunction(){ 
  return false;
}

double GeneratorSAN::Access5DstActivity_case2::SampleDistribution(){
  return 0;
}

double* GeneratorSAN::Access5DstActivity_case2::ReturnDistributionParameters(){
    return NULL;
}

int GeneratorSAN::Access5DstActivity_case2::Rank(){
  return 1;
}

BaseActionClass* GeneratorSAN::Access5DstActivity_case2::Fire(){
  (*(Access5_Mobius_Mark))--;
  Destination->Mark()=2;
  return this;
}

/*======================Access5DstActivity_case3========================*/


GeneratorSAN::Access5DstActivity_case3::Access5DstActivity_case3(){
  ActivityInitialize("Access5Dst_case3",8,Instantaneous , RaceEnabled, 2,1, false);
}

void GeneratorSAN::Access5DstActivity_case3::LinkVariables(){
  Access5->Register(&Access5_Mobius_Mark);

}

bool GeneratorSAN::Access5DstActivity_case3::Enabled(){
  OldEnabled=NewEnabled;
  NewEnabled=(((*(Access5_Mobius_Mark)) >=1));
  return NewEnabled;
}

double GeneratorSAN::Access5DstActivity_case3::Weight(){ 
  return d3/(d1+d2+d3);
}

bool GeneratorSAN::Access5DstActivity_case3::ReactivationPredicate(){ 
  return false;
}

bool GeneratorSAN::Access5DstActivity_case3::ReactivationFunction(){ 
  return false;
}

double GeneratorSAN::Access5DstActivity_case3::SampleDistribution(){
  return 0;
}

double* GeneratorSAN::Access5DstActivity_case3::ReturnDistributionParameters(){
    return NULL;
}

int GeneratorSAN::Access5DstActivity_case3::Rank(){
  return 1;
}

BaseActionClass* GeneratorSAN::Access5DstActivity_case3::Fire(){
  (*(Access5_Mobius_Mark))--;
  Destination->Mark()=3;
  return this;
}

/*======================Access6DstActivity_case1========================*/


GeneratorSAN::Access6DstActivity_case1::Access6DstActivity_case1(){
  ActivityInitialize("Access6Dst_case1",9,Instantaneous , RaceEnabled, 2,1, false);
}

void GeneratorSAN::Access6DstActivity_case1::LinkVariables(){
  Access6->Register(&Access6_Mobius_Mark);

}

bool GeneratorSAN::Access6DstActivity_case1::Enabled(){
  OldEnabled=NewEnabled;
  NewEnabled=(((*(Access6_Mobius_Mark)) >=1));
  return NewEnabled;
}

double GeneratorSAN::Access6DstActivity_case1::Weight(){ 
  return d1/(d1+d2+d3);
}

bool GeneratorSAN::Access6DstActivity_case1::ReactivationPredicate(){ 
  return false;
}

bool GeneratorSAN::Access6DstActivity_case1::ReactivationFunction(){ 
  return false;
}

double GeneratorSAN::Access6DstActivity_case1::SampleDistribution(){
  return 0;
}

double* GeneratorSAN::Access6DstActivity_case1::ReturnDistributionParameters(){
    return NULL;
}

int GeneratorSAN::Access6DstActivity_case1::Rank(){
  return 1;
}

BaseActionClass* GeneratorSAN::Access6DstActivity_case1::Fire(){
  (*(Access6_Mobius_Mark))--;
  Destination->Mark()=1;
  return this;
}

/*======================Access6DstActivity_case2========================*/


GeneratorSAN::Access6DstActivity_case2::Access6DstActivity_case2(){
  ActivityInitialize("Access6Dst_case2",9,Instantaneous , RaceEnabled, 2,1, false);
}

void GeneratorSAN::Access6DstActivity_case2::LinkVariables(){
  Access6->Register(&Access6_Mobius_Mark);

}

bool GeneratorSAN::Access6DstActivity_case2::Enabled(){
  OldEnabled=NewEnabled;
  NewEnabled=(((*(Access6_Mobius_Mark)) >=1));
  return NewEnabled;
}

double GeneratorSAN::Access6DstActivity_case2::Weight(){ 
  return d2/(d1+d2+d3);
}

bool GeneratorSAN::Access6DstActivity_case2::ReactivationPredicate(){ 
  return false;
}

bool GeneratorSAN::Access6DstActivity_case2::ReactivationFunction(){ 
  return false;
}

double GeneratorSAN::Access6DstActivity_case2::SampleDistribution(){
  return 0;
}

double* GeneratorSAN::Access6DstActivity_case2::ReturnDistributionParameters(){
    return NULL;
}

int GeneratorSAN::Access6DstActivity_case2::Rank(){
  return 1;
}

BaseActionClass* GeneratorSAN::Access6DstActivity_case2::Fire(){
  (*(Access6_Mobius_Mark))--;
  Destination->Mark()=2;
  return this;
}

/*======================Access6DstActivity_case3========================*/


GeneratorSAN::Access6DstActivity_case3::Access6DstActivity_case3(){
  ActivityInitialize("Access6Dst_case3",9,Instantaneous , RaceEnabled, 2,1, false);
}

void GeneratorSAN::Access6DstActivity_case3::LinkVariables(){
  Access6->Register(&Access6_Mobius_Mark);

}

bool GeneratorSAN::Access6DstActivity_case3::Enabled(){
  OldEnabled=NewEnabled;
  NewEnabled=(((*(Access6_Mobius_Mark)) >=1));
  return NewEnabled;
}

double GeneratorSAN::Access6DstActivity_case3::Weight(){ 
  return d3/(d1+d2+d3);
}

bool GeneratorSAN::Access6DstActivity_case3::ReactivationPredicate(){ 
  return false;
}

bool GeneratorSAN::Access6DstActivity_case3::ReactivationFunction(){ 
  return false;
}

double GeneratorSAN::Access6DstActivity_case3::SampleDistribution(){
  return 0;
}

double* GeneratorSAN::Access6DstActivity_case3::ReturnDistributionParameters(){
    return NULL;
}

int GeneratorSAN::Access6DstActivity_case3::Rank(){
  return 1;
}

BaseActionClass* GeneratorSAN::Access6DstActivity_case3::Fire(){
  (*(Access6_Mobius_Mark))--;
  Destination->Mark()=3;
  return this;
}

/*======================ClockActivity========================*/

GeneratorSAN::ClockActivity::ClockActivity(){
  TheDistributionParameters = new double[1];
  ActivityInitialize("Clock",0,Exponential, RaceEnabled, 2,1, false);
}

GeneratorSAN::ClockActivity::~ClockActivity(){
  delete[] TheDistributionParameters;
}

void GeneratorSAN::ClockActivity::LinkVariables(){
  Arrival->Register(&Arrival_Mobius_Mark);
  Packet->Register(&Packet_Mobius_Mark);
}

bool GeneratorSAN::ClockActivity::Enabled(){
  OldEnabled=NewEnabled;
  NewEnabled=(((*(Arrival_Mobius_Mark)) >=1));
  return NewEnabled;
}

double GeneratorSAN::ClockActivity::Rate(){
  return 1;
  return 1.0;  // default rate if none is specified
}

double GeneratorSAN::ClockActivity::Weight(){ 
  return 1;
}

bool GeneratorSAN::ClockActivity::ReactivationPredicate(){ 
  return false;
}

bool GeneratorSAN::ClockActivity::ReactivationFunction(){ 
  return false;
}

double GeneratorSAN::ClockActivity::SampleDistribution(){
  return TheDistribution->Exponential(1);
}

double* GeneratorSAN::ClockActivity::ReturnDistributionParameters(){
  TheDistributionParameters[0] = Rate();
  return TheDistributionParameters;
}

int GeneratorSAN::ClockActivity::Rank(){
  return 1;
}

BaseActionClass* GeneratorSAN::ClockActivity::Fire(){
  (*(Arrival_Mobius_Mark))--;
  (*(Packet_Mobius_Mark))++;
  return this;
}

