#ifndef GeneratorSAN_H_
#define GeneratorSAN_H_

#include "Cpp/BaseClasses/EmptyGroup.h"
#include "Cpp/BaseClasses/PreselectGroup.h"
#include "Cpp/BaseClasses/PostselectGroup.h"
#include "Cpp/BaseClasses/state/StructStateVariable.h"
#include "Cpp/BaseClasses/state/ArrayStateVariable.h"
#include "Cpp/BaseClasses/SAN/SANModel.h" 
#include "Cpp/BaseClasses/SAN/Place.h"
#include "Cpp/BaseClasses/SAN/ExtendedPlace.h"
extern double s1;
extern double s2;
extern double s3;
extern double s4;
extern double s5;
extern double s6;
extern double d1;
extern double d2;
extern double d3;
extern UserDistributions* TheDistribution;

void MemoryError();

#ifndef _Src_Hist_header_
#define _Src_Hist_header_

typedef short Src_Hist_state;
class Src_Hist: public ArrayStateVariable<ExtendedPlace<short> > {
  public:
  Src_Hist(char* name, char* fullname):
    ArrayStateVariable<ExtendedPlace<short> >(name, fullname, Short ,3) {
    char varname[12];
    char fqname[strlen(fullname) + strlen(name) + 2];
    sprintf(fqname, "%s%s", fullname, name);    for (int i=0;i<3;i++) {
      sprintf(varname,"%d",i);
      fields.push_back(new ExtendedPlace<short>(varname, fqname));
    }
  }

  Src_Hist(char* name):
    ArrayStateVariable<ExtendedPlace<short> >(name, "", Short ,3) {
    char varname[12];
    for (int i=0;i<3;i++) {
      sprintf(varname,"%d",i);
      fields.push_back(new ExtendedPlace<short>(varname, name));
    }
  }

  Src_Hist(char* name, char* fullname, short & initialValue):
    ArrayStateVariable<ExtendedPlace<short> >(name, fullname, Short ,3) {
    char varname[12];
    char fqname[strlen(fullname) + strlen(name) + 2];
    sprintf(fqname, "%s%s", fullname, name);    for (int i=0;i<3;i++) {
      sprintf(varname,"%d",i);
      fields.push_back(new ExtendedPlace<short>(varname, fqname, initialValue));
    }
  }

  Src_Hist(char* name, short & initialValue):
    ArrayStateVariable<ExtendedPlace<short> >(name, "", Short ,3) {
    char varname[12];
    for (int i=0;i<3;i++) {
      sprintf(varname,"%d",i);
      fields.push_back(new ExtendedPlace<short>(varname, name, initialValue));
    }
  }

  Src_Hist(char* name, char* fullname, short * initialValue):
    ArrayStateVariable<ExtendedPlace<short> >(name, fullname, Short ,3) {
    char varname[12];
    char fqname[strlen(fullname) + strlen(name) + 2];
    sprintf(fqname, "%s%s", fullname, name);    for (int i=0;i<3;i++) {
      sprintf(varname,"%d",i);
      fields.push_back(new ExtendedPlace<short>(varname, fqname, initialValue[i]));
    }
  }

  Src_Hist(char* name, short * initialValue):
    ArrayStateVariable<ExtendedPlace<short> >(name, "", Short ,3) {
    char varname[12];
    for (int i=0;i<3;i++) {
      sprintf(varname,"%d",i);
      fields.push_back(new ExtendedPlace<short>(varname, name, initialValue[i]));
    }
  }
  ~Src_Hist() {
    for (fieldIterator i=fields.begin();i!=fields.end();++i)
      delete (*i);
  }
};
#endif
#ifndef _Dst_Hist_header_
#define _Dst_Hist_header_

typedef short Dst_Hist_state;
class Dst_Hist: public ArrayStateVariable<ExtendedPlace<short> > {
  public:
  Dst_Hist(char* name, char* fullname):
    ArrayStateVariable<ExtendedPlace<short> >(name, fullname, Short ,6) {
    char varname[12];
    char fqname[strlen(fullname) + strlen(name) + 2];
    sprintf(fqname, "%s%s", fullname, name);    for (int i=0;i<6;i++) {
      sprintf(varname,"%d",i);
      fields.push_back(new ExtendedPlace<short>(varname, fqname));
    }
  }

  Dst_Hist(char* name):
    ArrayStateVariable<ExtendedPlace<short> >(name, "", Short ,6) {
    char varname[12];
    for (int i=0;i<6;i++) {
      sprintf(varname,"%d",i);
      fields.push_back(new ExtendedPlace<short>(varname, name));
    }
  }

  Dst_Hist(char* name, char* fullname, short & initialValue):
    ArrayStateVariable<ExtendedPlace<short> >(name, fullname, Short ,6) {
    char varname[12];
    char fqname[strlen(fullname) + strlen(name) + 2];
    sprintf(fqname, "%s%s", fullname, name);    for (int i=0;i<6;i++) {
      sprintf(varname,"%d",i);
      fields.push_back(new ExtendedPlace<short>(varname, fqname, initialValue));
    }
  }

  Dst_Hist(char* name, short & initialValue):
    ArrayStateVariable<ExtendedPlace<short> >(name, "", Short ,6) {
    char varname[12];
    for (int i=0;i<6;i++) {
      sprintf(varname,"%d",i);
      fields.push_back(new ExtendedPlace<short>(varname, name, initialValue));
    }
  }

  Dst_Hist(char* name, char* fullname, short * initialValue):
    ArrayStateVariable<ExtendedPlace<short> >(name, fullname, Short ,6) {
    char varname[12];
    char fqname[strlen(fullname) + strlen(name) + 2];
    sprintf(fqname, "%s%s", fullname, name);    for (int i=0;i<6;i++) {
      sprintf(varname,"%d",i);
      fields.push_back(new ExtendedPlace<short>(varname, fqname, initialValue[i]));
    }
  }

  Dst_Hist(char* name, short * initialValue):
    ArrayStateVariable<ExtendedPlace<short> >(name, "", Short ,6) {
    char varname[12];
    for (int i=0;i<6;i++) {
      sprintf(varname,"%d",i);
      fields.push_back(new ExtendedPlace<short>(varname, name, initialValue[i]));
    }
  }
  ~Dst_Hist() {
    for (fieldIterator i=fields.begin();i!=fields.end();++i)
      delete (*i);
  }
};
#endif

/*********************************************************************
               GeneratorSAN Submodel Definition                   
*********************************************************************/

class GeneratorSAN:public SANModel{
public:

class SourceTypeActivity_case1:public Activity {
public:

  Place* Packet;
  short* Packet_Mobius_Mark;
  Place* GoodFlag;
  short* GoodFlag_Mobius_Mark;
  Place* BadFlag;
  short* BadFlag_Mobius_Mark;
  Place* GoodPacket;
  short* GoodPacket_Mobius_Mark;

  double* TheDistributionParameters;
  SourceTypeActivity_case1();
  double Rate(){return 0;}
  bool Enabled();
  void LinkVariables();
  double Weight();
  bool ReactivationPredicate();
  bool ReactivationFunction();
  double SampleDistribution();
  double* ReturnDistributionParameters();
  int Rank();
  BaseActionClass* Fire();
}; // SourceTypeActivity_case1Activity

class SourceTypeActivity_case2:public Activity {
public:

  Place* Packet;
  short* Packet_Mobius_Mark;
  Place* BadFlag;
  short* BadFlag_Mobius_Mark;
  Place* GoodFlag;
  short* GoodFlag_Mobius_Mark;
  Place* BadPacket;
  short* BadPacket_Mobius_Mark;

  double* TheDistributionParameters;
  SourceTypeActivity_case2();
  double Rate(){return 0;}
  bool Enabled();
  void LinkVariables();
  double Weight();
  bool ReactivationPredicate();
  bool ReactivationFunction();
  double SampleDistribution();
  double* ReturnDistributionParameters();
  int Rank();
  BaseActionClass* Fire();
}; // SourceTypeActivity_case2Activity

class BadPacketSourceActivity_case1:public Activity {
public:

  Place* BadPacket;
  short* BadPacket_Mobius_Mark;
  ExtendedPlace<short>* Source;
  Place* Access5;
  short* Access5_Mobius_Mark;

  double* TheDistributionParameters;
  BadPacketSourceActivity_case1();
  double Rate(){return 0;}
  bool Enabled();
  void LinkVariables();
  double Weight();
  bool ReactivationPredicate();
  bool ReactivationFunction();
  double SampleDistribution();
  double* ReturnDistributionParameters();
  int Rank();
  BaseActionClass* Fire();
}; // BadPacketSourceActivity_case1Activity

class BadPacketSourceActivity_case2:public Activity {
public:

  Place* BadPacket;
  short* BadPacket_Mobius_Mark;
  ExtendedPlace<short>* Source;
  Place* Access6;
  short* Access6_Mobius_Mark;

  double* TheDistributionParameters;
  BadPacketSourceActivity_case2();
  double Rate(){return 0;}
  bool Enabled();
  void LinkVariables();
  double Weight();
  bool ReactivationPredicate();
  bool ReactivationFunction();
  double SampleDistribution();
  double* ReturnDistributionParameters();
  int Rank();
  BaseActionClass* Fire();
}; // BadPacketSourceActivity_case2Activity

class GoodPacketSourceActivity_case1:public Activity {
public:

  Place* GoodPacket;
  short* GoodPacket_Mobius_Mark;
  ExtendedPlace<short>* Source;
  Place* Access1;
  short* Access1_Mobius_Mark;

  double* TheDistributionParameters;
  GoodPacketSourceActivity_case1();
  double Rate(){return 0;}
  bool Enabled();
  void LinkVariables();
  double Weight();
  bool ReactivationPredicate();
  bool ReactivationFunction();
  double SampleDistribution();
  double* ReturnDistributionParameters();
  int Rank();
  BaseActionClass* Fire();
}; // GoodPacketSourceActivity_case1Activity

class GoodPacketSourceActivity_case2:public Activity {
public:

  Place* GoodPacket;
  short* GoodPacket_Mobius_Mark;
  ExtendedPlace<short>* Source;
  Place* Access2;
  short* Access2_Mobius_Mark;

  double* TheDistributionParameters;
  GoodPacketSourceActivity_case2();
  double Rate(){return 0;}
  bool Enabled();
  void LinkVariables();
  double Weight();
  bool ReactivationPredicate();
  bool ReactivationFunction();
  double SampleDistribution();
  double* ReturnDistributionParameters();
  int Rank();
  BaseActionClass* Fire();
}; // GoodPacketSourceActivity_case2Activity

class GoodPacketSourceActivity_case3:public Activity {
public:

  Place* GoodPacket;
  short* GoodPacket_Mobius_Mark;
  ExtendedPlace<short>* Source;
  Place* Access3;
  short* Access3_Mobius_Mark;

  double* TheDistributionParameters;
  GoodPacketSourceActivity_case3();
  double Rate(){return 0;}
  bool Enabled();
  void LinkVariables();
  double Weight();
  bool ReactivationPredicate();
  bool ReactivationFunction();
  double SampleDistribution();
  double* ReturnDistributionParameters();
  int Rank();
  BaseActionClass* Fire();
}; // GoodPacketSourceActivity_case3Activity

class GoodPacketSourceActivity_case4:public Activity {
public:

  Place* GoodPacket;
  short* GoodPacket_Mobius_Mark;
  ExtendedPlace<short>* Source;
  Place* Access4;
  short* Access4_Mobius_Mark;

  double* TheDistributionParameters;
  GoodPacketSourceActivity_case4();
  double Rate(){return 0;}
  bool Enabled();
  void LinkVariables();
  double Weight();
  bool ReactivationPredicate();
  bool ReactivationFunction();
  double SampleDistribution();
  double* ReturnDistributionParameters();
  int Rank();
  BaseActionClass* Fire();
}; // GoodPacketSourceActivity_case4Activity

class Access1DstActivity:public Activity {
public:

  Place* Access1;
  short* Access1_Mobius_Mark;
  ExtendedPlace<short>* Destination;

  double* TheDistributionParameters;
  Access1DstActivity();
  double Rate(){return 0;}
  bool Enabled();
  void LinkVariables();
  double Weight();
  bool ReactivationPredicate();
  bool ReactivationFunction();
  double SampleDistribution();
  double* ReturnDistributionParameters();
  int Rank();
  BaseActionClass* Fire();
}; // Access1DstActivityActivity

class Access2DstActivity:public Activity {
public:

  Place* Access2;
  short* Access2_Mobius_Mark;
  ExtendedPlace<short>* Destination;

  double* TheDistributionParameters;
  Access2DstActivity();
  double Rate(){return 0;}
  bool Enabled();
  void LinkVariables();
  double Weight();
  bool ReactivationPredicate();
  bool ReactivationFunction();
  double SampleDistribution();
  double* ReturnDistributionParameters();
  int Rank();
  BaseActionClass* Fire();
}; // Access2DstActivityActivity

class Access3DstActivity:public Activity {
public:

  Place* Access3;
  short* Access3_Mobius_Mark;
  ExtendedPlace<short>* Destination;

  double* TheDistributionParameters;
  Access3DstActivity();
  double Rate(){return 0;}
  bool Enabled();
  void LinkVariables();
  double Weight();
  bool ReactivationPredicate();
  bool ReactivationFunction();
  double SampleDistribution();
  double* ReturnDistributionParameters();
  int Rank();
  BaseActionClass* Fire();
}; // Access3DstActivityActivity

class Access4DstActivity:public Activity {
public:

  Place* Access4;
  short* Access4_Mobius_Mark;
  ExtendedPlace<short>* Destination;

  double* TheDistributionParameters;
  Access4DstActivity();
  double Rate(){return 0;}
  bool Enabled();
  void LinkVariables();
  double Weight();
  bool ReactivationPredicate();
  bool ReactivationFunction();
  double SampleDistribution();
  double* ReturnDistributionParameters();
  int Rank();
  BaseActionClass* Fire();
}; // Access4DstActivityActivity

class Access5DstActivity_case1:public Activity {
public:

  Place* Access5;
  short* Access5_Mobius_Mark;
  ExtendedPlace<short>* Destination;

  double* TheDistributionParameters;
  Access5DstActivity_case1();
  double Rate(){return 0;}
  bool Enabled();
  void LinkVariables();
  double Weight();
  bool ReactivationPredicate();
  bool ReactivationFunction();
  double SampleDistribution();
  double* ReturnDistributionParameters();
  int Rank();
  BaseActionClass* Fire();
}; // Access5DstActivity_case1Activity

class Access5DstActivity_case2:public Activity {
public:

  Place* Access5;
  short* Access5_Mobius_Mark;
  ExtendedPlace<short>* Destination;

  double* TheDistributionParameters;
  Access5DstActivity_case2();
  double Rate(){return 0;}
  bool Enabled();
  void LinkVariables();
  double Weight();
  bool ReactivationPredicate();
  bool ReactivationFunction();
  double SampleDistribution();
  double* ReturnDistributionParameters();
  int Rank();
  BaseActionClass* Fire();
}; // Access5DstActivity_case2Activity

class Access5DstActivity_case3:public Activity {
public:

  Place* Access5;
  short* Access5_Mobius_Mark;
  ExtendedPlace<short>* Destination;

  double* TheDistributionParameters;
  Access5DstActivity_case3();
  double Rate(){return 0;}
  bool Enabled();
  void LinkVariables();
  double Weight();
  bool ReactivationPredicate();
  bool ReactivationFunction();
  double SampleDistribution();
  double* ReturnDistributionParameters();
  int Rank();
  BaseActionClass* Fire();
}; // Access5DstActivity_case3Activity

class Access6DstActivity_case1:public Activity {
public:

  Place* Access6;
  short* Access6_Mobius_Mark;
  ExtendedPlace<short>* Destination;

  double* TheDistributionParameters;
  Access6DstActivity_case1();
  double Rate(){return 0;}
  bool Enabled();
  void LinkVariables();
  double Weight();
  bool ReactivationPredicate();
  bool ReactivationFunction();
  double SampleDistribution();
  double* ReturnDistributionParameters();
  int Rank();
  BaseActionClass* Fire();
}; // Access6DstActivity_case1Activity

class Access6DstActivity_case2:public Activity {
public:

  Place* Access6;
  short* Access6_Mobius_Mark;
  ExtendedPlace<short>* Destination;

  double* TheDistributionParameters;
  Access6DstActivity_case2();
  double Rate(){return 0;}
  bool Enabled();
  void LinkVariables();
  double Weight();
  bool ReactivationPredicate();
  bool ReactivationFunction();
  double SampleDistribution();
  double* ReturnDistributionParameters();
  int Rank();
  BaseActionClass* Fire();
}; // Access6DstActivity_case2Activity

class Access6DstActivity_case3:public Activity {
public:

  Place* Access6;
  short* Access6_Mobius_Mark;
  ExtendedPlace<short>* Destination;

  double* TheDistributionParameters;
  Access6DstActivity_case3();
  double Rate(){return 0;}
  bool Enabled();
  void LinkVariables();
  double Weight();
  bool ReactivationPredicate();
  bool ReactivationFunction();
  double SampleDistribution();
  double* ReturnDistributionParameters();
  int Rank();
  BaseActionClass* Fire();
}; // Access6DstActivity_case3Activity

class ClockActivity:public Activity {
public:

  Place* Arrival;
  short* Arrival_Mobius_Mark;
  Place* Packet;
  short* Packet_Mobius_Mark;

  double* TheDistributionParameters;
  ClockActivity();
  ~ClockActivity();
  bool Enabled();
  void LinkVariables();
  double Weight();
  bool ReactivationPredicate();
  bool ReactivationFunction();
  double SampleDistribution();
  double* ReturnDistributionParameters();
  int Rank();
  BaseActionClass* Fire();
  double Rate();
}; // ClockActivityActivity

  //List of user-specified place names
  Place* Arrival;
  Place* GoodPacket;
  Place* BadPacket;
  Place* GoodFlag;
  Place* BadFlag;
  Place* Packet;
  Place* Access1;
  Place* Access2;
  Place* Access3;
  Place* Access4;
  Place* Access5;
  Place* Access6;
  ExtendedPlace<short>* Source;
  ExtendedPlace<short>* Destination;

  // Create instances of all actvities
  SourceTypeActivity_case1 SourceType_case1;
  SourceTypeActivity_case2 SourceType_case2;
  BadPacketSourceActivity_case1 BadPacketSource_case1;
  BadPacketSourceActivity_case2 BadPacketSource_case2;
  GoodPacketSourceActivity_case1 GoodPacketSource_case1;
  GoodPacketSourceActivity_case2 GoodPacketSource_case2;
  GoodPacketSourceActivity_case3 GoodPacketSource_case3;
  GoodPacketSourceActivity_case4 GoodPacketSource_case4;
  Access1DstActivity Access1Dst;
  Access2DstActivity Access2Dst;
  Access3DstActivity Access3Dst;
  Access4DstActivity Access4Dst;
  Access5DstActivity_case1 Access5Dst_case1;
  Access5DstActivity_case2 Access5Dst_case2;
  Access5DstActivity_case3 Access5Dst_case3;
  Access6DstActivity_case1 Access6Dst_case1;
  Access6DstActivity_case2 Access6Dst_case2;
  Access6DstActivity_case3 Access6Dst_case3;
  ClockActivity Clock;
  //Create instances of all groups 
  PreselectGroup ImmediateGroup;
  PostselectGroup SourceTypeGroup;
  PostselectGroup BadPacketSourceGroup;
  PostselectGroup GoodPacketSourceGroup;
  PostselectGroup Access1DstGroup;
  PostselectGroup Access2DstGroup;
  PostselectGroup Access3DstGroup;
  PostselectGroup Access4DstGroup;
  PostselectGroup Access5DstGroup;
  PostselectGroup Access6DstGroup;

  GeneratorSAN();
  ~GeneratorSAN();
  void CustomInitialization();

  void assignPlacesToActivitiesInst();
  void assignPlacesToActivitiesTimed();
}; // end GeneratorSAN

#endif // GeneratorSAN_H_
