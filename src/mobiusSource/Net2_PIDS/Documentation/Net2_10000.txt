Mobius ars Version 2.4, 
Copyright (c) William H. Sanders and Univ. of Illinois 1994-2010

******************************************************************************
                      Mobius ACCUMULATED REWARD SOLVER 

                      ACCUMULATED REWARD SOLVER RESULTS
******************************************************************************

Started: Fri Nov  8 16:11:43 2013

Project name: Net2_PIDS
Study name: IdsStudy3
State space name: IdsSSG2
Experiment name: Experiment_1

Global variable settings for this experiment:
	d1 = 0.33
	d2 = 0.33
	d3 = 0.33
	s1 = 0.2
	s2 = 0.2
	s3 = 0.2
	s4 = 0.2
	s5 = 0.1
	s6 = 0.1

Verbosity: 0
Accuracy: 1.000000e-09
Time intervals:
    [0.000000,10000.000000]

Computation Time (seconds): 5.784000
User Time (seconds): 5.784000
System Time (seconds): -0.000000
Rate of Poisson process: 1.000000
Number of states in process: 1969
Number of states with reward: 1968
Number of time intervals: 1

      Time Point       Left Trunc.      # Iterations          Error     
   ---------------   ---------------   ---------------   ---------------
      10000.000000              9298             10992      1.000000e-09

------------------------------------------------------------------------------
Performance variable             :  TruePositive
Time interval                    :  [0.000000,10000.000000]
Expected accumulated reward      :  1.999493e+03
Time-averaged accumulated reward :  1.999493e-01

------------------------------------------------------------------------------
Performance variable             :  FalsePositive
Time interval                    :  [0.000000,10000.000000]
Expected accumulated reward      :  7.997905e+03
Time-averaged accumulated reward :  7.997905e-01

------------------------------------------------------------------------------
Performance variable             :  FalseNegative
Time interval                    :  [0.000000,10000.000000]
Expected accumulated reward      :  3.068182e-01
Time-averaged accumulated reward :  3.068182e-05

------------------------------------------------------------------------------
Performance variable             :  TrueNegative
Time interval                    :  [0.000000,10000.000000]
Expected accumulated reward      :  1.295455e+00
Time-averaged accumulated reward :  1.295455e-04

Finished: Fri Nov  8 16:11:49 2013

