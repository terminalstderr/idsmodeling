#ifndef _IDSREWARD3_PVS_
#define _IDSREWARD3_PVS_
#include <math.h>
#include "Cpp/Performance_Variables/PerformanceVariableNode.hpp"
#include "Composed/IDS/IDSRJ.h"
#include "Cpp/Performance_Variables/IntervalOfTime.hpp"


class IdsReward3PV0Worker:public IntervalOfTime
{
 public:
  GeneratorSAN *Generator;
  DetectorSAN *Detector;
  
  IdsReward3PV0Worker();
  ~IdsReward3PV0Worker();
  double Reward_Function();
};

class IdsReward3PV0:public PerformanceVariableNode
{
 public:
  IDSRJ *TheIDSRJ;

  IdsReward3PV0Worker *IdsReward3PV0WorkerList;

  IdsReward3PV0(int timeindex=0);
  ~IdsReward3PV0();
  void CreateWorkerList(void);
};

class IdsReward3PV1Worker:public IntervalOfTime
{
 public:
  GeneratorSAN *Generator;
  DetectorSAN *Detector;
  
  IdsReward3PV1Worker();
  ~IdsReward3PV1Worker();
  double Reward_Function();
};

class IdsReward3PV1:public PerformanceVariableNode
{
 public:
  IDSRJ *TheIDSRJ;

  IdsReward3PV1Worker *IdsReward3PV1WorkerList;

  IdsReward3PV1(int timeindex=0);
  ~IdsReward3PV1();
  void CreateWorkerList(void);
};

class IdsReward3PV2Worker:public IntervalOfTime
{
 public:
  GeneratorSAN *Generator;
  DetectorSAN *Detector;
  
  IdsReward3PV2Worker();
  ~IdsReward3PV2Worker();
  double Reward_Function();
};

class IdsReward3PV2:public PerformanceVariableNode
{
 public:
  IDSRJ *TheIDSRJ;

  IdsReward3PV2Worker *IdsReward3PV2WorkerList;

  IdsReward3PV2(int timeindex=0);
  ~IdsReward3PV2();
  void CreateWorkerList(void);
};

class IdsReward3PV3Worker:public IntervalOfTime
{
 public:
  GeneratorSAN *Generator;
  DetectorSAN *Detector;
  
  IdsReward3PV3Worker();
  ~IdsReward3PV3Worker();
  double Reward_Function();
};

class IdsReward3PV3:public PerformanceVariableNode
{
 public:
  IDSRJ *TheIDSRJ;

  IdsReward3PV3Worker *IdsReward3PV3WorkerList;

  IdsReward3PV3(int timeindex=0);
  ~IdsReward3PV3();
  void CreateWorkerList(void);
};

#endif
