#include "IdsReward3PVNodes.h"

IdsReward3PV0Worker::IdsReward3PV0Worker()
{
  NumModels = 2;
  TheModelPtr = new BaseModelClass**[NumModels];
  TheModelPtr[0] = (BaseModelClass**)(&Generator);
  TheModelPtr[1] = (BaseModelClass**)(&Detector);
}

IdsReward3PV0Worker::~IdsReward3PV0Worker() {
  delete [] TheModelPtr;
}

double IdsReward3PV0Worker::Reward_Function(void) {

if (Generator->BadFlag->Mark() == 1)
{
	if(Detector->Bad->Mark() == 1)
	{	
		return 1;
	}
}

return (0);



}

IdsReward3PV0::IdsReward3PV0(int timeindex) {
  TheModelPtr = (BaseModelClass**)(&TheIDSRJ);
  double startpts[1]={0.0};
  double stoppts[1]={100000};
  Initialize("TruePositive",(RewardType)1,1, startpts, stoppts, timeindex, 0,2, 2);
  AddVariableDependency("BadFlag","Generator");
  AddVariableDependency("Bad","Detector");
}

IdsReward3PV0::~IdsReward3PV0() {
  for(int i = 0; i < NumberOfWorkers; i++) {
    delete[] WorkerList[i]->Name;
    delete WorkerList[i];
  }
}

void IdsReward3PV0::CreateWorkerList(void) {
  for(int i = 0; i < NumberOfWorkers; i++)
    WorkerList[i] = new IdsReward3PV0Worker;
}
IdsReward3PV1Worker::IdsReward3PV1Worker()
{
  NumModels = 2;
  TheModelPtr = new BaseModelClass**[NumModels];
  TheModelPtr[0] = (BaseModelClass**)(&Generator);
  TheModelPtr[1] = (BaseModelClass**)(&Detector);
}

IdsReward3PV1Worker::~IdsReward3PV1Worker() {
  delete [] TheModelPtr;
}

double IdsReward3PV1Worker::Reward_Function(void) {

if (Generator->GoodFlag->Mark() == 1)
{
	if(Detector->Bad->Mark() == 1)
	{	
		return 1;
	}
}

return (0);



}

IdsReward3PV1::IdsReward3PV1(int timeindex) {
  TheModelPtr = (BaseModelClass**)(&TheIDSRJ);
  double startpts[1]={0.0};
  double stoppts[1]={100000};
  Initialize("FalsePositive",(RewardType)1,1, startpts, stoppts, timeindex, 0,2, 2);
  AddVariableDependency("GoodFlag","Generator");
  AddVariableDependency("Bad","Detector");
}

IdsReward3PV1::~IdsReward3PV1() {
  for(int i = 0; i < NumberOfWorkers; i++) {
    delete[] WorkerList[i]->Name;
    delete WorkerList[i];
  }
}

void IdsReward3PV1::CreateWorkerList(void) {
  for(int i = 0; i < NumberOfWorkers; i++)
    WorkerList[i] = new IdsReward3PV1Worker;
}
IdsReward3PV2Worker::IdsReward3PV2Worker()
{
  NumModels = 2;
  TheModelPtr = new BaseModelClass**[NumModels];
  TheModelPtr[0] = (BaseModelClass**)(&Generator);
  TheModelPtr[1] = (BaseModelClass**)(&Detector);
}

IdsReward3PV2Worker::~IdsReward3PV2Worker() {
  delete [] TheModelPtr;
}

double IdsReward3PV2Worker::Reward_Function(void) {

if (Generator->BadFlag->Mark() == 1)
{
	if(Detector->Good->Mark() == 1)
	{	
		return 1;
	}
}

return (0);



}

IdsReward3PV2::IdsReward3PV2(int timeindex) {
  TheModelPtr = (BaseModelClass**)(&TheIDSRJ);
  double startpts[1]={0.0};
  double stoppts[1]={100000};
  Initialize("FalseNegative",(RewardType)1,1, startpts, stoppts, timeindex, 0,2, 2);
  AddVariableDependency("BadFlag","Generator");
  AddVariableDependency("Good","Detector");
}

IdsReward3PV2::~IdsReward3PV2() {
  for(int i = 0; i < NumberOfWorkers; i++) {
    delete[] WorkerList[i]->Name;
    delete WorkerList[i];
  }
}

void IdsReward3PV2::CreateWorkerList(void) {
  for(int i = 0; i < NumberOfWorkers; i++)
    WorkerList[i] = new IdsReward3PV2Worker;
}
IdsReward3PV3Worker::IdsReward3PV3Worker()
{
  NumModels = 2;
  TheModelPtr = new BaseModelClass**[NumModels];
  TheModelPtr[0] = (BaseModelClass**)(&Generator);
  TheModelPtr[1] = (BaseModelClass**)(&Detector);
}

IdsReward3PV3Worker::~IdsReward3PV3Worker() {
  delete [] TheModelPtr;
}

double IdsReward3PV3Worker::Reward_Function(void) {

if (Generator->GoodFlag->Mark() == 1)
{
	if(Detector->Good->Mark() == 1)
	{	
		return 1;
	}
}

return (0);



}

IdsReward3PV3::IdsReward3PV3(int timeindex) {
  TheModelPtr = (BaseModelClass**)(&TheIDSRJ);
  double startpts[1]={0.0};
  double stoppts[1]={100000};
  Initialize("TrueNegative",(RewardType)1,1, startpts, stoppts, timeindex, 0,2, 2);
  AddVariableDependency("GoodFlag","Generator");
  AddVariableDependency("Good","Detector");
}

IdsReward3PV3::~IdsReward3PV3() {
  for(int i = 0; i < NumberOfWorkers; i++) {
    delete[] WorkerList[i]->Name;
    delete WorkerList[i];
  }
}

void IdsReward3PV3::CreateWorkerList(void) {
  for(int i = 0; i < NumberOfWorkers; i++)
    WorkerList[i] = new IdsReward3PV3Worker;
}
