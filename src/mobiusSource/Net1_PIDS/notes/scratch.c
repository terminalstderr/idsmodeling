double pNorm, pAtt;
double e = 2.718;

int numSrc = 2;     //we are considering 2 sources and 2 destinations
int numDst = 2;

int numDst1_acc = 0;    //number of different sources who access destination 1
int numDst2_acc = 0;    //number of different sources who access destination 2

int i;

for(i=0; i<numSrc; i++) {
    numDst1_acc += Dst1_Hist->Mark()[i];
    numDst2_acc += Dst2_Hist->Mark()[i];
}
int totAccesses = numDst1_acc + numDst2_acc;    //number of total different accesses

//we will need nCk formula and we are going to calculate factorials of n, k, (n-k)
int numDst_fact = numDst;

//calculating factorials (ugly way)
for(i=numDst-1; i>0; i--)
    numDst_fact *= i;

if(numDst_fact == 0)
    numDst_fact = 1;


if(Source->Mark() == 1) {
    int numSrc1_acc = 0;    //number of different destinations accessed by source 1

     for(i=0; i<numDst; i++) {
        numSrc1_acc += Src1_Hist->Mark()[i];
     }  

    //again for nCk and factorials
    int numSrc1_acc_fact = numSrc1_acc;


    for(i=numSrc1_acc-1; i>0; i--)
        numSrc1_acc_fact *= i;

    
    if(numSrc1_acc_fact == 0)
        numSrc1_acc_fact = 1;

    int diff1_fact = numDst - numSrc1_acc;

    for(i=diff1_fact-1; i>0; i--)
        diff1_fact *= i;

    if(diff1_fact == 0)
        diff1_fact = 1;


    double k1 = (numDst_fact)/(numSrc1_acc_fact * diff1_fact * pow((double) numDst, (double) numSrc1_acc));

    pNorm =   1/(k1 * (e-1) * numSrc1_acc_fact);

    if(Src1_Hist->Mark()[0] ==1)
          pNorm *= (numDst1_acc )/(totAccesses);
    
    if(Src1_Hist->Mark()[1] ==1)
          pNorm *= (numDst2_acc )/(totAccesses);

    pAtt = 1/(k1 * pow((double) numDst, (double) (numSrc1_acc +1)));

}

if(Source->Mark() == 2) {
    int numSrc2_acc = 0;

    for(i=0; i<numDst; i++) {
        numSrc2_acc += Src2_Hist->Mark()[i];
    }   

    int numSrc2_acc_fact = numSrc2_acc;
    int diff2_fact = numDst - numSrc2_acc;

    for(i=numSrc2_acc_fact-1; i>0; i--)
        numSrc2_acc_fact *= i;

    for(i=diff2_fact-1; i>0; i--)
        diff2_acc_fact *= i;

    double k2 = (numDst_fact)/(numSrc2_acc_fact * diff2_fact * pow((double) numDst, (double) numSrc2_acc));

    pNorm =   1/(k2 * (e-1) * numSrc2_acc_fact);

    if(Src2_Hist->Mark()[0] ==1)
          pNorm *= (numDst1_acc )/(totAccesses);
    
    if(Src2_Hist->Mark()[1] ==1)
          pNorm *= (numDst2_acc )/(totAccesses);

    pAtt = 1/(k2 * pow((double) numDst, (double) (numSrc2_acc +1)));
}


// General Decisions
if(pNorm > pAtt) { // Detected Packet Good
    Good->Mark() = 1;
    Bad->Mark() = 0;
}

else { // Detected Packet Bad
    Good->Mark() = 0;
    Bad->Mark() = 1;
}

// Model Logic
Source->Mark() = 0;
Destination->Mark() = 0;

