#include "Composed/IDS/IDSRJ.h"
char * IDSRJ__SharedNames[] = {"Arrival", "Destination", "Source"};

IDSRJ::IDSRJ():Join("Join", 2, 3,IDSRJ__SharedNames) {
  Generator = new GeneratorSAN();
  ModelArray[0] = (BaseModelClass*) Generator;
  ModelArray[0]->DefineName("Generator");
  Detector = new DetectorSAN();
  ModelArray[1] = (BaseModelClass*) Detector;
  ModelArray[1]->DefineName("Detector");

  SetupActions();
  if (AllChildrenEmpty())
    NumSharedStateVariables = 0;
  else {
    //**************  State sharing info  **************
    //Shared variable 0
    Arrival = new Place("Arrival");
    addSharedPtr(Arrival, "Arrival" );
    if (Generator->NumStateVariables > 0) {
      Arrival->ShareWith(Generator->Arrival);
      addSharingInfo(Generator->Arrival, Arrival, Generator);
    }
    if (Detector->NumStateVariables > 0) {
      Arrival->ShareWith(Detector->Departure);
      addSharingInfo(Detector->Departure, Arrival, Detector);
    }

    //Shared variable 1
    Destination = new Place("Destination");
    addSharedPtr(Destination, "Destination" );
    if (Generator->NumStateVariables > 0) {
      Destination->ShareWith(Generator->Destination);
      addSharingInfo(Generator->Destination, Destination, Generator);
    }
    if (Detector->NumStateVariables > 0) {
      Destination->ShareWith(Detector->Destination);
      addSharingInfo(Detector->Destination, Destination, Detector);
    }

    //Shared variable 2
    Source = new Place("Source");
    addSharedPtr(Source, "Source" );
    if (Generator->NumStateVariables > 0) {
      Source->ShareWith(Generator->Source);
      addSharingInfo(Generator->Source, Source, Generator);
    }
    if (Detector->NumStateVariables > 0) {
      Source->ShareWith(Detector->Source);
      addSharingInfo(Detector->Source, Source, Detector);
    }

  }

  Setup();
}

IDSRJ::~IDSRJ() {
  if (!AllChildrenEmpty()) {
    delete Arrival;
    delete Destination;
    delete Source;
  }
  delete Generator;
  delete Detector;
}
