#ifndef _IDSREWARD2_PVS_
#define _IDSREWARD2_PVS_
#include <math.h>
#include "Cpp/Performance_Variables/PerformanceVariableNode.hpp"
#include "Composed/IDS/IDSRJ.h"
#include "Cpp/Performance_Variables/SteadyState.hpp"


class IdsReward2PV0Worker:public SteadyState
{
 public:
  GeneratorSAN *Generator;
  DetectorSAN *Detector;
  
  IdsReward2PV0Worker();
  ~IdsReward2PV0Worker();
  double Reward_Function();
};

class IdsReward2PV0:public PerformanceVariableNode
{
 public:
  IDSRJ *TheIDSRJ;

  IdsReward2PV0Worker *IdsReward2PV0WorkerList;

  IdsReward2PV0(int timeindex=0);
  ~IdsReward2PV0();
  void CreateWorkerList(void);
};

class IdsReward2PV1Worker:public SteadyState
{
 public:
  GeneratorSAN *Generator;
  DetectorSAN *Detector;
  
  IdsReward2PV1Worker();
  ~IdsReward2PV1Worker();
  double Reward_Function();
};

class IdsReward2PV1:public PerformanceVariableNode
{
 public:
  IDSRJ *TheIDSRJ;

  IdsReward2PV1Worker *IdsReward2PV1WorkerList;

  IdsReward2PV1(int timeindex=0);
  ~IdsReward2PV1();
  void CreateWorkerList(void);
};

class IdsReward2PV2Worker:public SteadyState
{
 public:
  GeneratorSAN *Generator;
  DetectorSAN *Detector;
  
  IdsReward2PV2Worker();
  ~IdsReward2PV2Worker();
  double Reward_Function();
};

class IdsReward2PV2:public PerformanceVariableNode
{
 public:
  IDSRJ *TheIDSRJ;

  IdsReward2PV2Worker *IdsReward2PV2WorkerList;

  IdsReward2PV2(int timeindex=0);
  ~IdsReward2PV2();
  void CreateWorkerList(void);
};

class IdsReward2PV3Worker:public SteadyState
{
 public:
  GeneratorSAN *Generator;
  DetectorSAN *Detector;
  
  IdsReward2PV3Worker();
  ~IdsReward2PV3Worker();
  double Reward_Function();
};

class IdsReward2PV3:public PerformanceVariableNode
{
 public:
  IDSRJ *TheIDSRJ;

  IdsReward2PV3Worker *IdsReward2PV3WorkerList;

  IdsReward2PV3(int timeindex=0);
  ~IdsReward2PV3();
  void CreateWorkerList(void);
};

#endif
