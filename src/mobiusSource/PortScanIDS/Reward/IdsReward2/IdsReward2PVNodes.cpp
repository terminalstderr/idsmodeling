#include "IdsReward2PVNodes.h"

IdsReward2PV0Worker::IdsReward2PV0Worker()
{
  NumModels = 2;
  TheModelPtr = new BaseModelClass**[NumModels];
  TheModelPtr[0] = (BaseModelClass**)(&Generator);
  TheModelPtr[1] = (BaseModelClass**)(&Detector);
}

IdsReward2PV0Worker::~IdsReward2PV0Worker() {
  delete [] TheModelPtr;
}

double IdsReward2PV0Worker::Reward_Function(void) {

if (Generator->BadFlag->Mark() == 1)
{
	if(Detector->Bad->Mark() == 1)
	{	
		return 1;
	}
}

return (0);



}

IdsReward2PV0::IdsReward2PV0(int timeindex) {
  TheModelPtr = (BaseModelClass**)(&TheIDSRJ);
  double startpts[1]={100.0};
  double stoppts[1]={100.0+1.0};
  Initialize("TruePositive",(RewardType)3,1, startpts, stoppts, timeindex, 0,2, 2);
  Type = steady_state;
  AddVariableDependency("BadFlag","Generator");
  AddVariableDependency("Bad","Detector");
}

IdsReward2PV0::~IdsReward2PV0() {
  for(int i = 0; i < NumberOfWorkers; i++) {
    delete[] WorkerList[i]->Name;
    delete WorkerList[i];
  }
}

void IdsReward2PV0::CreateWorkerList(void) {
  for(int i = 0; i < NumberOfWorkers; i++)
    WorkerList[i] = new IdsReward2PV0Worker;
}
IdsReward2PV1Worker::IdsReward2PV1Worker()
{
  NumModels = 2;
  TheModelPtr = new BaseModelClass**[NumModels];
  TheModelPtr[0] = (BaseModelClass**)(&Generator);
  TheModelPtr[1] = (BaseModelClass**)(&Detector);
}

IdsReward2PV1Worker::~IdsReward2PV1Worker() {
  delete [] TheModelPtr;
}

double IdsReward2PV1Worker::Reward_Function(void) {

if (Generator->GoodFlag->Mark() == 1)
{
	if(Detector->Bad->Mark() == 1)
	{	
		return 1;
	}
}

return (0);



}

IdsReward2PV1::IdsReward2PV1(int timeindex) {
  TheModelPtr = (BaseModelClass**)(&TheIDSRJ);
  double startpts[1]={100.0};
  double stoppts[1]={100.0+1.0};
  Initialize("FalsePositive",(RewardType)3,1, startpts, stoppts, timeindex, 0,2, 2);
  Type = steady_state;
  AddVariableDependency("GoodFlag","Generator");
  AddVariableDependency("Bad","Detector");
}

IdsReward2PV1::~IdsReward2PV1() {
  for(int i = 0; i < NumberOfWorkers; i++) {
    delete[] WorkerList[i]->Name;
    delete WorkerList[i];
  }
}

void IdsReward2PV1::CreateWorkerList(void) {
  for(int i = 0; i < NumberOfWorkers; i++)
    WorkerList[i] = new IdsReward2PV1Worker;
}
IdsReward2PV2Worker::IdsReward2PV2Worker()
{
  NumModels = 2;
  TheModelPtr = new BaseModelClass**[NumModels];
  TheModelPtr[0] = (BaseModelClass**)(&Generator);
  TheModelPtr[1] = (BaseModelClass**)(&Detector);
}

IdsReward2PV2Worker::~IdsReward2PV2Worker() {
  delete [] TheModelPtr;
}

double IdsReward2PV2Worker::Reward_Function(void) {

if (Generator->BadFlag->Mark() == 1)
{
	if(Detector->Good->Mark() == 1)
	{	
		return 1;
	}
}

return (0);



}

IdsReward2PV2::IdsReward2PV2(int timeindex) {
  TheModelPtr = (BaseModelClass**)(&TheIDSRJ);
  double startpts[1]={100.0};
  double stoppts[1]={100.0+1.0};
  Initialize("FalseNegative",(RewardType)3,1, startpts, stoppts, timeindex, 0,2, 2);
  Type = steady_state;
  AddVariableDependency("BadFlag","Generator");
  AddVariableDependency("Good","Detector");
}

IdsReward2PV2::~IdsReward2PV2() {
  for(int i = 0; i < NumberOfWorkers; i++) {
    delete[] WorkerList[i]->Name;
    delete WorkerList[i];
  }
}

void IdsReward2PV2::CreateWorkerList(void) {
  for(int i = 0; i < NumberOfWorkers; i++)
    WorkerList[i] = new IdsReward2PV2Worker;
}
IdsReward2PV3Worker::IdsReward2PV3Worker()
{
  NumModels = 2;
  TheModelPtr = new BaseModelClass**[NumModels];
  TheModelPtr[0] = (BaseModelClass**)(&Generator);
  TheModelPtr[1] = (BaseModelClass**)(&Detector);
}

IdsReward2PV3Worker::~IdsReward2PV3Worker() {
  delete [] TheModelPtr;
}

double IdsReward2PV3Worker::Reward_Function(void) {

if (Generator->GoodFlag->Mark() == 1)
{
	if(Detector->Good->Mark() == 1)
	{	
		return 1;
	}
}

return (0);



}

IdsReward2PV3::IdsReward2PV3(int timeindex) {
  TheModelPtr = (BaseModelClass**)(&TheIDSRJ);
  double startpts[1]={100.0};
  double stoppts[1]={100.0+1.0};
  Initialize("TrueNegative",(RewardType)3,1, startpts, stoppts, timeindex, 0,2, 2);
  Type = steady_state;
  AddVariableDependency("GoodFlag","Generator");
  AddVariableDependency("Good","Detector");
}

IdsReward2PV3::~IdsReward2PV3() {
  for(int i = 0; i < NumberOfWorkers; i++) {
    delete[] WorkerList[i]->Name;
    delete WorkerList[i];
  }
}

void IdsReward2PV3::CreateWorkerList(void) {
  for(int i = 0; i < NumberOfWorkers; i++)
    WorkerList[i] = new IdsReward2PV3Worker;
}
