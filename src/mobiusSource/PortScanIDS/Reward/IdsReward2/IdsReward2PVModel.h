#ifndef _IDSREWARD2_PVMODEL_
#define _IDSREWARD2_PVMODEL_
#include "IdsReward2PVNodes.h"
#include "Cpp/Performance_Variables/PVModel.hpp"
#include "Composed/IDS/IDSRJ.h"
class IdsReward2PVModel:public PVModel {
 protected:
  PerformanceVariableNode *createPVNode(int pvindex, int timeindex);
 public:
  IdsReward2PVModel(bool expandtimepoints);
};

#endif
