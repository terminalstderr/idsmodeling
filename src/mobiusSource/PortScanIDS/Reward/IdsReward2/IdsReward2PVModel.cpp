#include "IdsReward2PVModel.h"

IdsReward2PVModel::IdsReward2PVModel(bool expandTimeArrays) {
  TheModel=new IDSRJ();
  DefineName("IdsReward2PVModel");
  CreatePVList(4, expandTimeArrays);
  Initialize();
}



PerformanceVariableNode* IdsReward2PVModel::createPVNode(int pvindex, int timeindex) {
  switch(pvindex) {
  case 0:
    return new IdsReward2PV0(timeindex);
    break;
  case 1:
    return new IdsReward2PV1(timeindex);
    break;
  case 2:
    return new IdsReward2PV2(timeindex);
    break;
  case 3:
    return new IdsReward2PV3(timeindex);
    break;
  }
  return NULL;
}
