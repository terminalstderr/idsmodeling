#ifndef _IDSREWARD3_PVMODEL_
#define _IDSREWARD3_PVMODEL_
#include "IdsReward3PVNodes.h"
#include "Cpp/Performance_Variables/PVModel.hpp"
#include "Composed/IDS/IDSRJ.h"
class IdsReward3PVModel:public PVModel {
 protected:
  PerformanceVariableNode *createPVNode(int pvindex, int timeindex);
 public:
  IdsReward3PVModel(bool expandtimepoints);
};

#endif
