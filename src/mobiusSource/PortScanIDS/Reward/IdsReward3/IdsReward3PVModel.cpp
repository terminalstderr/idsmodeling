#include "IdsReward3PVModel.h"

IdsReward3PVModel::IdsReward3PVModel(bool expandTimeArrays) {
  TheModel=new IDSRJ();
  DefineName("IdsReward3PVModel");
  CreatePVList(4, expandTimeArrays);
  Initialize();
}



PerformanceVariableNode* IdsReward3PVModel::createPVNode(int pvindex, int timeindex) {
  switch(pvindex) {
  case 0:
    return new IdsReward3PV0(timeindex);
    break;
  case 1:
    return new IdsReward3PV1(timeindex);
    break;
  case 2:
    return new IdsReward3PV2(timeindex);
    break;
  case 3:
    return new IdsReward3PV3(timeindex);
    break;
  }
  return NULL;
}
