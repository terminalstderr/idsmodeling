
#ifndef IdsStudy2RangeSTUDY_H_
#define IdsStudy2RangeSTUDY_H_

#include "Reward/IdsReward2/IdsReward2PVNodes.h"
#include "Reward/IdsReward2/IdsReward2PVModel.h"
#include "Cpp/Study/BaseStudyClass.hpp"

extern double p_src1;
extern double p_src2;

class IdsStudy2RangeStudy : public BaseStudyClass {
public:

IdsStudy2RangeStudy();
~IdsStudy2RangeStudy();

private:

double *p_src1Values;
double *p_src2Values;

void SetValues_p_src1();
void SetValues_p_src2();

void PrintGlobalValues(int);
void *GetGVValue(char *TheGVName);
void OverrideGVValue(char *TheGVName, void *TheGVValue);
void SetGVs(int expnum);
PVModel *GetPVModel(bool expandTimeArrays);
};

#endif

