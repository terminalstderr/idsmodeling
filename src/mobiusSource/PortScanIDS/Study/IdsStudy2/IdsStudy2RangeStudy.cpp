
#include "Study/IdsStudy2/IdsStudy2RangeStudy.h"

//******************************************************
//Global Variables
//******************************************************
double p_src1;
double p_src2;

//********************************************************
//IdsStudy2RangeStudy Constructor
//********************************************************
IdsStudy2RangeStudy::IdsStudy2RangeStudy() {

  // define arrays of global variable names and types
  NumGVs = 2;
  NumExps = 1;

  GVNames = new char*[NumGVs];
  GVTypes = new char*[NumGVs];
  GVNames[0]=strdup("p_src1");
  GVTypes[0]=strdup("double");
  GVNames[1]=strdup("p_src2");
  GVTypes[1]=strdup("double");

  // create the arrays to store the values of each gv
  p_src1Values = new double[NumExps];
  p_src2Values = new double[NumExps];

  // call methods to assign values to each gv
  SetValues_p_src1();
  SetValues_p_src2();
  SetDefaultMobiusRoot(MOBIUSROOT);
}


//******************************************************
//IdsStudy2RangeStudy Destructor
//******************************************************
IdsStudy2RangeStudy::~IdsStudy2RangeStudy() {
  delete [] p_src1Values;
  delete [] p_src2Values;
  delete ThePVModel;
}


//******************************************************
// set values for p_src1
//******************************************************
void IdsStudy2RangeStudy::SetValues_p_src1() {
  p_src1Values[0] = 0.2;
}


//******************************************************
// set values for p_src2
//******************************************************
void IdsStudy2RangeStudy::SetValues_p_src2() {
  p_src2Values[0] = 0.8;
}



//******************************************************
//print values of gv (for debugging)
//******************************************************
void IdsStudy2RangeStudy::PrintGlobalValues(int expNum) {
  if (NumGVs == 0) {
    cout<<"There are no global variables."<<endl;
    return;
  }

  SetGVs(expNum);

  cout<<"The Global Variable values for experiment "<<
    GetExpName(expNum)<<" are:"<<endl;
  cout << "p_src1\tdouble\t" << p_src1 << endl;
  cout << "p_src2\tdouble\t" << p_src2 << endl;
}


//******************************************************
//retrieve the value of a global variable
//******************************************************
void *IdsStudy2RangeStudy::GetGVValue(char *TheGVName) {
  if (strcmp("p_src1", TheGVName) == 0)
    return &p_src1;
  else if (strcmp("p_src2", TheGVName) == 0)
    return &p_src2;
  else 
    cerr<<"!! IdsStudy2RangeStudy::GetGVValue: Global Variable "<<TheGVName<<" does not exist."<<endl;
  return NULL;
}


//******************************************************
//override the value of a global variable
//******************************************************
void IdsStudy2RangeStudy::OverrideGVValue(char *TheGVName,void *TheGVValue) {
  if (strcmp("p_src1", TheGVName) == 0)
    p_src1 = *(double *)TheGVValue;
  else if (strcmp("p_src2", TheGVName) == 0)
    p_src2 = *(double *)TheGVValue;
  else 
    cerr<<"!! IdsStudy2RangeStudy::OverrideGVValue: Global Variable "<<TheGVName<<" does not exist."<<endl;
}


//******************************************************
//set the value of all global variables to the given exp
//******************************************************
void IdsStudy2RangeStudy::SetGVs(int expNum) {
  p_src1 = p_src1Values[expNum];
  p_src2 = p_src2Values[expNum];
}


//******************************************************
//static class method called by solvers to create study 
//(and thus create all of the model)
//******************************************************
BaseStudyClass* GlobalStudyPtr = NULL;
BaseStudyClass * GenerateStudy() {
  if (GlobalStudyPtr == NULL)
    GlobalStudyPtr = new IdsStudy2RangeStudy();
  return GlobalStudyPtr;
}

void DestructStudy() {
  delete GlobalStudyPtr;
  GlobalStudyPtr = NULL;
}
//******************************************************
//get and create the PVModel
//******************************************************
PVModel* IdsStudy2RangeStudy::GetPVModel(bool expandTimeArrays) {
  if (ThePVModel!=NULL)
    delete ThePVModel;
  // create the PV model
  ThePVModel=new IdsReward2PVModel(expandTimeArrays);
  return ThePVModel;
}


