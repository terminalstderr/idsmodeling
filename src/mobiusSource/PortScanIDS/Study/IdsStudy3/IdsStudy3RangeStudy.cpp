
#include "Study/IdsStudy3/IdsStudy3RangeStudy.h"

//******************************************************
//Global Variables
//******************************************************
double d1;
double d2;
double d3;
double s1;
double s2;
double s3;
double s4;
double s5;
double s6;

//********************************************************
//IdsStudy3RangeStudy Constructor
//********************************************************
IdsStudy3RangeStudy::IdsStudy3RangeStudy() {

  // define arrays of global variable names and types
  NumGVs = 9;
  NumExps = 1;

  GVNames = new char*[NumGVs];
  GVTypes = new char*[NumGVs];
  GVNames[0]=strdup("d1");
  GVTypes[0]=strdup("double");
  GVNames[1]=strdup("d2");
  GVTypes[1]=strdup("double");
  GVNames[2]=strdup("d3");
  GVTypes[2]=strdup("double");
  GVNames[3]=strdup("s1");
  GVTypes[3]=strdup("double");
  GVNames[4]=strdup("s2");
  GVTypes[4]=strdup("double");
  GVNames[5]=strdup("s3");
  GVTypes[5]=strdup("double");
  GVNames[6]=strdup("s4");
  GVTypes[6]=strdup("double");
  GVNames[7]=strdup("s5");
  GVTypes[7]=strdup("double");
  GVNames[8]=strdup("s6");
  GVTypes[8]=strdup("double");

  // create the arrays to store the values of each gv
  d1Values = new double[NumExps];
  d2Values = new double[NumExps];
  d3Values = new double[NumExps];
  s1Values = new double[NumExps];
  s2Values = new double[NumExps];
  s3Values = new double[NumExps];
  s4Values = new double[NumExps];
  s5Values = new double[NumExps];
  s6Values = new double[NumExps];

  // call methods to assign values to each gv
  SetValues_d1();
  SetValues_d2();
  SetValues_d3();
  SetValues_s1();
  SetValues_s2();
  SetValues_s3();
  SetValues_s4();
  SetValues_s5();
  SetValues_s6();
  SetDefaultMobiusRoot(MOBIUSROOT);
}


//******************************************************
//IdsStudy3RangeStudy Destructor
//******************************************************
IdsStudy3RangeStudy::~IdsStudy3RangeStudy() {
  delete [] d1Values;
  delete [] d2Values;
  delete [] d3Values;
  delete [] s1Values;
  delete [] s2Values;
  delete [] s3Values;
  delete [] s4Values;
  delete [] s5Values;
  delete [] s6Values;
  delete ThePVModel;
}


//******************************************************
// set values for d1
//******************************************************
void IdsStudy3RangeStudy::SetValues_d1() {
  d1Values[0] = 0.33;
}


//******************************************************
// set values for d2
//******************************************************
void IdsStudy3RangeStudy::SetValues_d2() {
  d2Values[0] = 0.33;
}


//******************************************************
// set values for d3
//******************************************************
void IdsStudy3RangeStudy::SetValues_d3() {
  d3Values[0] = 0.33;
}


//******************************************************
// set values for s1
//******************************************************
void IdsStudy3RangeStudy::SetValues_s1() {
  s1Values[0] = 0.2;
}


//******************************************************
// set values for s2
//******************************************************
void IdsStudy3RangeStudy::SetValues_s2() {
  s2Values[0] = 0.2;
}


//******************************************************
// set values for s3
//******************************************************
void IdsStudy3RangeStudy::SetValues_s3() {
  s3Values[0] = 0.2;
}


//******************************************************
// set values for s4
//******************************************************
void IdsStudy3RangeStudy::SetValues_s4() {
  s4Values[0] = 0.2;
}


//******************************************************
// set values for s5
//******************************************************
void IdsStudy3RangeStudy::SetValues_s5() {
  s5Values[0] = 0.1;
}


//******************************************************
// set values for s6
//******************************************************
void IdsStudy3RangeStudy::SetValues_s6() {
  s6Values[0] = 0.1;
}



//******************************************************
//print values of gv (for debugging)
//******************************************************
void IdsStudy3RangeStudy::PrintGlobalValues(int expNum) {
  if (NumGVs == 0) {
    cout<<"There are no global variables."<<endl;
    return;
  }

  SetGVs(expNum);

  cout<<"The Global Variable values for experiment "<<
    GetExpName(expNum)<<" are:"<<endl;
  cout << "d1\tdouble\t" << d1 << endl;
  cout << "d2\tdouble\t" << d2 << endl;
  cout << "d3\tdouble\t" << d3 << endl;
  cout << "s1\tdouble\t" << s1 << endl;
  cout << "s2\tdouble\t" << s2 << endl;
  cout << "s3\tdouble\t" << s3 << endl;
  cout << "s4\tdouble\t" << s4 << endl;
  cout << "s5\tdouble\t" << s5 << endl;
  cout << "s6\tdouble\t" << s6 << endl;
}


//******************************************************
//retrieve the value of a global variable
//******************************************************
void *IdsStudy3RangeStudy::GetGVValue(char *TheGVName) {
  if (strcmp("d1", TheGVName) == 0)
    return &d1;
  else if (strcmp("d2", TheGVName) == 0)
    return &d2;
  else if (strcmp("d3", TheGVName) == 0)
    return &d3;
  else if (strcmp("s1", TheGVName) == 0)
    return &s1;
  else if (strcmp("s2", TheGVName) == 0)
    return &s2;
  else if (strcmp("s3", TheGVName) == 0)
    return &s3;
  else if (strcmp("s4", TheGVName) == 0)
    return &s4;
  else if (strcmp("s5", TheGVName) == 0)
    return &s5;
  else if (strcmp("s6", TheGVName) == 0)
    return &s6;
  else 
    cerr<<"!! IdsStudy3RangeStudy::GetGVValue: Global Variable "<<TheGVName<<" does not exist."<<endl;
  return NULL;
}


//******************************************************
//override the value of a global variable
//******************************************************
void IdsStudy3RangeStudy::OverrideGVValue(char *TheGVName,void *TheGVValue) {
  if (strcmp("d1", TheGVName) == 0)
    d1 = *(double *)TheGVValue;
  else if (strcmp("d2", TheGVName) == 0)
    d2 = *(double *)TheGVValue;
  else if (strcmp("d3", TheGVName) == 0)
    d3 = *(double *)TheGVValue;
  else if (strcmp("s1", TheGVName) == 0)
    s1 = *(double *)TheGVValue;
  else if (strcmp("s2", TheGVName) == 0)
    s2 = *(double *)TheGVValue;
  else if (strcmp("s3", TheGVName) == 0)
    s3 = *(double *)TheGVValue;
  else if (strcmp("s4", TheGVName) == 0)
    s4 = *(double *)TheGVValue;
  else if (strcmp("s5", TheGVName) == 0)
    s5 = *(double *)TheGVValue;
  else if (strcmp("s6", TheGVName) == 0)
    s6 = *(double *)TheGVValue;
  else 
    cerr<<"!! IdsStudy3RangeStudy::OverrideGVValue: Global Variable "<<TheGVName<<" does not exist."<<endl;
}


//******************************************************
//set the value of all global variables to the given exp
//******************************************************
void IdsStudy3RangeStudy::SetGVs(int expNum) {
  d1 = d1Values[expNum];
  d2 = d2Values[expNum];
  d3 = d3Values[expNum];
  s1 = s1Values[expNum];
  s2 = s2Values[expNum];
  s3 = s3Values[expNum];
  s4 = s4Values[expNum];
  s5 = s5Values[expNum];
  s6 = s6Values[expNum];
}


//******************************************************
//static class method called by solvers to create study 
//(and thus create all of the model)
//******************************************************
BaseStudyClass* GlobalStudyPtr = NULL;
BaseStudyClass * GenerateStudy() {
  if (GlobalStudyPtr == NULL)
    GlobalStudyPtr = new IdsStudy3RangeStudy();
  return GlobalStudyPtr;
}

void DestructStudy() {
  delete GlobalStudyPtr;
  GlobalStudyPtr = NULL;
}
//******************************************************
//get and create the PVModel
//******************************************************
PVModel* IdsStudy3RangeStudy::GetPVModel(bool expandTimeArrays) {
  if (ThePVModel!=NULL)
    delete ThePVModel;
  // create the PV model
  ThePVModel=new IdsReward3PVModel(expandTimeArrays);
  return ThePVModel;
}


