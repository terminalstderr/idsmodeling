
#ifndef IdsStudy3RangeSTUDY_H_
#define IdsStudy3RangeSTUDY_H_

#include "Reward/IdsReward3/IdsReward3PVNodes.h"
#include "Reward/IdsReward3/IdsReward3PVModel.h"
#include "Cpp/Study/BaseStudyClass.hpp"

extern double d1;
extern double d2;
extern double d3;
extern double s1;
extern double s2;
extern double s3;
extern double s4;
extern double s5;
extern double s6;

class IdsStudy3RangeStudy : public BaseStudyClass {
public:

IdsStudy3RangeStudy();
~IdsStudy3RangeStudy();

private:

double *d1Values;
double *d2Values;
double *d3Values;
double *s1Values;
double *s2Values;
double *s3Values;
double *s4Values;
double *s5Values;
double *s6Values;

void SetValues_d1();
void SetValues_d2();
void SetValues_d3();
void SetValues_s1();
void SetValues_s2();
void SetValues_s3();
void SetValues_s4();
void SetValues_s5();
void SetValues_s6();

void PrintGlobalValues(int);
void *GetGVValue(char *TheGVName);
void OverrideGVValue(char *TheGVName, void *TheGVValue);
void SetGVs(int expnum);
PVModel *GetPVModel(bool expandTimeArrays);
};

#endif

