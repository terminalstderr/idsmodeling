

// This C++ file was created by SanEditor

#include "Atomic/Detector/DetectorSAN.h"

#include <stdlib.h>
#include <iostream>

#include <math.h>


/*****************************************************************
                         DetectorSAN Constructor             
******************************************************************/


DetectorSAN::DetectorSAN(){


  Activity* InitialActionList[1]={
    &Packet  // 0
  };

  BaseGroupClass* InitialGroupList[1]={
    (BaseGroupClass*) &(Packet)
  };

  Departure = new Place("Departure" ,1);
  Good = new Place("Good" ,0);
  Bad = new Place("Bad" ,0);
  short temp_Sourceshort = 0;
  Source = new ExtendedPlace<short>("Source",temp_Sourceshort);
  short temp_Destinationshort = 0;
  Destination = new ExtendedPlace<short>("Destination",temp_Destinationshort);
  short temp_Src1_HistSrc_Histvalue = 0;
  Src1_Hist = new Src_Hist("Src1_Hist",temp_Src1_HistSrc_Histvalue);
  short temp_Src2_HistSrc_Histvalue = 0;
  Src2_Hist = new Src_Hist("Src2_Hist",temp_Src2_HistSrc_Histvalue);
  short temp_Dst1_HistDst_Histvalue = 0;
  Dst1_Hist = new Dst_Hist("Dst1_Hist",temp_Dst1_HistDst_Histvalue);
  short temp_Dst2_HistDst_Histvalue = 0;
  Dst2_Hist = new Dst_Hist("Dst2_Hist",temp_Dst2_HistDst_Histvalue);
  short temp_Src3_HistSrc_Histvalue = 0;
  Src3_Hist = new Src_Hist("Src3_Hist",temp_Src3_HistSrc_Histvalue);
  short temp_Src4_HistSrc_Histvalue = 0;
  Src4_Hist = new Src_Hist("Src4_Hist",temp_Src4_HistSrc_Histvalue);
  short temp_Dst3_HistDst_Histvalue = 0;
  Dst3_Hist = new Dst_Hist("Dst3_Hist",temp_Dst3_HistDst_Histvalue);
  short temp_Src5_HistSrc_Histvalue = 0;
  Src5_Hist = new Src_Hist("Src5_Hist",temp_Src5_HistSrc_Histvalue);
  short temp_Src6_HistSrc_Histvalue = 0;
  Src6_Hist = new Src_Hist("Src6_Hist",temp_Src6_HistSrc_Histvalue);
  BaseStateVariableClass* InitialPlaces[14]={
    Departure,  // 0
    Good,  // 1
    Bad,  // 2
    Source,  // 3
    Destination,  // 4
    Src1_Hist,  // 5
    Src2_Hist,  // 6
    Dst1_Hist,  // 7
    Dst2_Hist,  // 8
    Src3_Hist,  // 9
    Src4_Hist,  // 10
    Dst3_Hist,  // 11
    Src5_Hist,  // 12
    Src6_Hist   // 13
  };
  BaseStateVariableClass* InitialROPlaces[0]={
  };
  initializeSANModelNow("Detector", 14, InitialPlaces, 
                        0, InitialROPlaces, 
                        1, InitialActionList, 1, InitialGroupList);


  assignPlacesToActivitiesInst();
  assignPlacesToActivitiesTimed();

  int AffectArcs[14][2]={ 
    {3,0}, {4,0}, {5,0}, {6,0}, {7,0}, {8,0}, {1,0}, {2,0}, {0,0}, 
    {11,0}, {9,0}, {10,0}, {12,0}, {13,0}
  };
  for(int n=0;n<14;n++) {
    AddAffectArc(InitialPlaces[AffectArcs[n][0]],
                 InitialActionList[AffectArcs[n][1]]);
  }
  int EnableArcs[2][2]={ 
    {3,0}, {4,0}
  };
  for(int n=0;n<2;n++) {
    AddEnableArc(InitialPlaces[EnableArcs[n][0]],
                 InitialActionList[EnableArcs[n][1]]);
  }

  for(int n=0;n<1;n++) {
    InitialActionList[n]->LinkVariables();
  }
  CustomInitialization();

}

void DetectorSAN::CustomInitialization() {

}
DetectorSAN::~DetectorSAN(){
  for (int i = 0; i < NumStateVariables-NumReadOnlyPlaces; i++)
    delete LocalStateVariables[i];
};

void DetectorSAN::assignPlacesToActivitiesInst(){
  Packet.Source = (ExtendedPlace<short>*) LocalStateVariables[3];
  Packet.Destination = (ExtendedPlace<short>*) LocalStateVariables[4];
  Packet.Src1_Hist = (Src_Hist*) LocalStateVariables[5];
  Packet.Src2_Hist = (Src_Hist*) LocalStateVariables[6];
  Packet.Dst1_Hist = (Dst_Hist*) LocalStateVariables[7];
  Packet.Dst2_Hist = (Dst_Hist*) LocalStateVariables[8];
  Packet.Good = (Place*) LocalStateVariables[1];
  Packet.Bad = (Place*) LocalStateVariables[2];
  Packet.Departure = (Place*) LocalStateVariables[0];
  Packet.Dst3_Hist = (Dst_Hist*) LocalStateVariables[11];
  Packet.Src3_Hist = (Src_Hist*) LocalStateVariables[9];
  Packet.Src4_Hist = (Src_Hist*) LocalStateVariables[10];
  Packet.Src5_Hist = (Src_Hist*) LocalStateVariables[12];
  Packet.Src6_Hist = (Src_Hist*) LocalStateVariables[13];
}
void DetectorSAN::assignPlacesToActivitiesTimed(){
}
/*****************************************************************/
/*                  Activity Method Definitions                  */
/*****************************************************************/

/*======================PacketActivity========================*/


DetectorSAN::PacketActivity::PacketActivity(){
  ActivityInitialize("Packet",0,Instantaneous , RaceEnabled, 14,2, false);
}

void DetectorSAN::PacketActivity::LinkVariables(){






  Good->Register(&Good_Mobius_Mark);
  Bad->Register(&Bad_Mobius_Mark);
  Departure->Register(&Departure_Mobius_Mark);





}

bool DetectorSAN::PacketActivity::Enabled(){
  OldEnabled=NewEnabled;
  NewEnabled=((Source->Mark()!=0 && Destination->Mark()!=0));
  return NewEnabled;
}

double DetectorSAN::PacketActivity::Weight(){ 
  return 1;
}

bool DetectorSAN::PacketActivity::ReactivationPredicate(){ 
  return false;
}

bool DetectorSAN::PacketActivity::ReactivationFunction(){ 
  return false;
}

double DetectorSAN::PacketActivity::SampleDistribution(){
  return 0;
}

double* DetectorSAN::PacketActivity::ReturnDistributionParameters(){
    return NULL;
}

int DetectorSAN::PacketActivity::Rank(){
  return 1;
}

BaseActionClass* DetectorSAN::PacketActivity::Fire(){
  
// Parsing Logic
// ---
// Setting Source History
// ---
if (Source->Mark() == 1)
{
	Src1_Hist->Index(Destination->Mark()-1)->Mark() = (short) 1;
}
else // (Source->Mark() == 2)
{
	Src2_Hist->Index(Destination->Mark()-1)->Mark() = 1;
}


// ---
// Setting Destination History
// ---
if (Destination->Mark() == 1)
{
	Dst1_Hist->Index(Source->Mark() - 1)->Mark() = 1;
}
else // (Destination->Mark() == 2)
{
	Dst2_Hist->Index(Source->Mark() - 1)->Mark() = 1;
}

  double pNorm, pAtt;
double e = 2.718;

int numSrc = 6; //we are considering 2 sources and 2 destinations
int numDst = 3;

int numDst1_acc = 0;	//number of different sources who access destination 1
int numDst2_acc = 0;	//number of different sources who access destination 2
int numDst3_acc = 0;


int i;

for(i=0; i<numSrc; i++) {
	numDst1_acc += Dst1_Hist->Index(i)->Mark();
	numDst2_acc += Dst2_Hist->Index(i)->Mark();
	numDst3_acc += Dst3_Hist->Index(i)->Mark();
}

int totAccesses = numDst1_acc + numDst2_acc + numDst3_acc;	//number of total different accesses


//we will need nCk formula and we are going to calculate factorials of n, k, (n-k)
int numDst_fact = numDst;	//factorial of |D|

//calculating factorials (ugly way)
for(i=numDst-1; i>0; i--)
numDst_fact *= i;

if(numDst_fact == 0)
numDst_fact = 1;


if(Source->Mark() == 1) {
 	int numSrc1_acc = 0;	//number of different destinations accessed by source 1

	for(i=0; i<numDst; i++) {
 		numSrc1_acc += Src1_Hist->Index(i)->Mark();
	}	

	//again for nCk and factorials
	int numSrc1_acc_fact = numSrc1_acc;


	for(i=numSrc1_acc-1; i>0; i--)
		numSrc1_acc_fact *= i;

	if(numSrc1_acc_fact == 0)
		numSrc1_acc_fact = 1;


	//double k1 = (numDst_fact)/(numSrc1_acc_fact * diff1_fact * pow((double) numDst, (double) numSrc1_acc));

	//pNorm =   1/(k1 * (e-1) * numSrc1_acc_fact);
	pNorm =   1/((e-1) * numSrc1_acc_fact);
	if(Src1_Hist->Index(0)->Mark() ==1)
 		pNorm *= (numDst1_acc )/(totAccesses);
	if(Src1_Hist->Index(1)->Mark() ==1)
 		pNorm *= (numDst2_acc )/(totAccesses);

	if(Src1_Hist->Index(2)->Mark() ==1)
		pNorm *= (numDst3_acc )/(totAccesses); 

	//pAtt = 1/(k1 * pow((double) numDst, (double) (numSrc1_acc +1)));
	pAtt = 1/(pow((double) numDst, (double) (numSrc1_acc +1)));
}

if(Source->Mark() == 2) {
	int numSrc2_acc = 0;

	for(i=0; i<numDst; i++) {
		numSrc2_acc += Src2_Hist->Index(i)->Mark();
 	}	

 	int numSrc2_acc_fact = numSrc2_acc;
 	int diff2_fact = numDst - numSrc2_acc;

 	for(i=numSrc2_acc_fact-1; i>0; i--)
		numSrc2_acc_fact *= i;

	for(i=diff2_fact-1; i>0; i--)
		diff2_fact *= i;

	//double k2 = (numDst_fact)/(numSrc2_acc_fact * diff2_fact * pow((double) numDst, (double) numSrc2_acc));

 	//pNorm =   1/(k2 * (e-1) * numSrc2_acc_fact);
	pNorm =   1/((e-1) * numSrc2_acc_fact);

	if(Src2_Hist->Index(0)->Mark() ==1)
		pNorm *= (numDst1_acc )/(totAccesses);
	if(Src2_Hist->Index(1)->Mark() ==1)
		pNorm *= (numDst2_acc )/(totAccesses);

	if(Src2_Hist->Index(2)->Mark() ==1)
 		pNorm *= (numDst3_acc )/(totAccesses);


 	//pAtt = 1/(k2 * pow((double) numDst, (double) (numSrc2_acc +1)));
	pAtt = 1/(pow((double) numDst, (double) (numSrc2_acc +1)));
}


if(Source->Mark() == 3) {
 	int numSrc3_acc = 0;	//number of different destinations accessed by source 3

	for(i=0; i<numDst; i++) {
 		numSrc3_acc += Src3_Hist->Index(i)->Mark();
	}	

	//again for nCk and factorials
	int numSrc3_acc_fact = numSrc3_acc;


	for(i=numSrc3_acc-1; i>0; i--)
		numSrc3_acc_fact *= i;

	if(numSrc3_acc_fact == 0)
		numSrc3_acc_fact = 1;

	pNorm =   1/((e-1) * numSrc3_acc_fact);

	if(Src3_Hist->Index(0)->Mark() ==1)
 		pNorm *= (numDst1_acc )/(totAccesses);
	if(Src3_Hist->Index(1)->Mark() ==1)
 		pNorm *= (numDst2_acc )/(totAccesses);
	if(Src3_Hist->Index(2)->Mark() ==1)
		pNorm *= (numDst3_acc )/(totAccesses);

	pAtt = 1/(pow((double) numDst, (double) (numSrc3_acc +1)));

}

if(Source->Mark() == 4) {
 	int numSrc4_acc = 0;	//number of different destinations accessed by source 3

	for(i=0; i<numDst; i++) {
 		numSrc4_acc += Src4_Hist->Index(i)->Mark();
	}	

	//again for nCk and factorials
	int numSrc4_acc_fact = numSrc4_acc;


	for(i=numSrc4_acc-1; i>0; i--)
		numSrc4_acc_fact *= i;
	
	if(numSrc4_acc_fact == 0)
		numSrc4_acc_fact = 1;

	pNorm =   1/((e-1) * numSrc4_acc_fact);

	if(Src4_Hist->Index(0)->Mark() ==1)
 		pNorm *= (numDst1_acc )/(totAccesses);
	if(Src4_Hist->Index(1)->Mark() ==1)
 		pNorm *= (numDst2_acc )/(totAccesses);
	if(Src4_Hist->Index(2)->Mark() ==1)
 		pNorm *= (numDst3_acc )/(totAccesses);

	pAtt = 1/(pow((double) numDst, (double) (numSrc4_acc +1)));

}

if(Source->Mark() == 5) {
 	int numSrc5_acc = 0;	//number of different destinations accessed by source 3

	for(i=0; i<numDst; i++) {
 		numSrc5_acc += Src5_Hist->Index(i)->Mark();
	}	

	//again for nCk and factorials
	int numSrc5_acc_fact = numSrc5_acc;


	for(i=numSrc5_acc-1; i>0; i--)
		numSrc5_acc_fact *= i;

	if(numSrc5_acc_fact == 0)
		numSrc5_acc_fact = 1;

	pNorm =   1/((e-1) * numSrc5_acc_fact);

	if(Src5_Hist->Index(0)->Mark() ==1)
 		pNorm *= (numDst1_acc )/(totAccesses);
	if(Src5_Hist->Index(1)->Mark() ==1)
		pNorm *= (numDst2_acc )/(totAccesses);
	if(Src5_Hist->Index(2)->Mark() ==1)
 		pNorm *= (numDst3_acc )/(totAccesses);

	pAtt = 1/(pow((double) numDst, (double) (numSrc5_acc +1)));

}

if(Source->Mark() == 6) {
 	int numSrc6_acc = 0;	//number of different destinations accessed by source 3

	for(i=0; i<numDst; i++) {
 		numSrc6_acc += Src6_Hist->Index(i)->Mark();
	}	

	//again for nCk and factorials
	int numSrc6_acc_fact = numSrc6_acc;


	for(i=numSrc6_acc-1; i>0; i--)
		numSrc6_acc_fact *= i;

	if(numSrc6_acc_fact == 0)
		numSrc6_acc_fact = 1;

	pNorm =   1/((e-1) * numSrc6_acc_fact);

	if(Src6_Hist->Index(0)->Mark() ==1)
 		pNorm *= (numDst1_acc )/(totAccesses);
	if(Src6_Hist->Index(1)->Mark() ==1)
 		pNorm *= (numDst2_acc )/(totAccesses);
	if(Src6_Hist->Index(2)->Mark() ==1)
 		pNorm *= (numDst3_acc )/(totAccesses);

	pAtt = 1/(pow((double) numDst, (double) (numSrc6_acc +1)));

}

// General Decisions
if(pNorm > pAtt) { // Detected Packet Good
Good->Mark() = 1;
Bad->Mark() = 0;
}

else { // Detected Packet Bad
Good->Mark() = 0;
Bad->Mark() = 1;
}


// ---
// Model Logic
// --
Source->Mark() = 0;
Destination->Mark() = 0;
Departure->Mark() = 1;


  return this;
}

