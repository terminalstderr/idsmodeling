#ifndef DetectorSAN_H_
#define DetectorSAN_H_

#include "Cpp/BaseClasses/EmptyGroup.h"
#include "Cpp/BaseClasses/PreselectGroup.h"
#include "Cpp/BaseClasses/PostselectGroup.h"
#include "Cpp/BaseClasses/state/StructStateVariable.h"
#include "Cpp/BaseClasses/state/ArrayStateVariable.h"
#include "Cpp/BaseClasses/SAN/SANModel.h" 
#include "Cpp/BaseClasses/SAN/Place.h"
#include "Cpp/BaseClasses/SAN/ExtendedPlace.h"
extern UserDistributions* TheDistribution;

void MemoryError();

#ifndef _Src_Hist_header_
#define _Src_Hist_header_

typedef short Src_Hist_state;
class Src_Hist: public ArrayStateVariable<ExtendedPlace<short> > {
  public:
  Src_Hist(char* name, char* fullname):
    ArrayStateVariable<ExtendedPlace<short> >(name, fullname, Short ,3) {
    char varname[12];
    char fqname[strlen(fullname) + strlen(name) + 2];
    sprintf(fqname, "%s%s", fullname, name);    for (int i=0;i<3;i++) {
      sprintf(varname,"%d",i);
      fields.push_back(new ExtendedPlace<short>(varname, fqname));
    }
  }

  Src_Hist(char* name):
    ArrayStateVariable<ExtendedPlace<short> >(name, "", Short ,3) {
    char varname[12];
    for (int i=0;i<3;i++) {
      sprintf(varname,"%d",i);
      fields.push_back(new ExtendedPlace<short>(varname, name));
    }
  }

  Src_Hist(char* name, char* fullname, short & initialValue):
    ArrayStateVariable<ExtendedPlace<short> >(name, fullname, Short ,3) {
    char varname[12];
    char fqname[strlen(fullname) + strlen(name) + 2];
    sprintf(fqname, "%s%s", fullname, name);    for (int i=0;i<3;i++) {
      sprintf(varname,"%d",i);
      fields.push_back(new ExtendedPlace<short>(varname, fqname, initialValue));
    }
  }

  Src_Hist(char* name, short & initialValue):
    ArrayStateVariable<ExtendedPlace<short> >(name, "", Short ,3) {
    char varname[12];
    for (int i=0;i<3;i++) {
      sprintf(varname,"%d",i);
      fields.push_back(new ExtendedPlace<short>(varname, name, initialValue));
    }
  }

  Src_Hist(char* name, char* fullname, short * initialValue):
    ArrayStateVariable<ExtendedPlace<short> >(name, fullname, Short ,3) {
    char varname[12];
    char fqname[strlen(fullname) + strlen(name) + 2];
    sprintf(fqname, "%s%s", fullname, name);    for (int i=0;i<3;i++) {
      sprintf(varname,"%d",i);
      fields.push_back(new ExtendedPlace<short>(varname, fqname, initialValue[i]));
    }
  }

  Src_Hist(char* name, short * initialValue):
    ArrayStateVariable<ExtendedPlace<short> >(name, "", Short ,3) {
    char varname[12];
    for (int i=0;i<3;i++) {
      sprintf(varname,"%d",i);
      fields.push_back(new ExtendedPlace<short>(varname, name, initialValue[i]));
    }
  }
  ~Src_Hist() {
    for (fieldIterator i=fields.begin();i!=fields.end();++i)
      delete (*i);
  }
};
#endif
#ifndef _Dst_Hist_header_
#define _Dst_Hist_header_

typedef short Dst_Hist_state;
class Dst_Hist: public ArrayStateVariable<ExtendedPlace<short> > {
  public:
  Dst_Hist(char* name, char* fullname):
    ArrayStateVariable<ExtendedPlace<short> >(name, fullname, Short ,6) {
    char varname[12];
    char fqname[strlen(fullname) + strlen(name) + 2];
    sprintf(fqname, "%s%s", fullname, name);    for (int i=0;i<6;i++) {
      sprintf(varname,"%d",i);
      fields.push_back(new ExtendedPlace<short>(varname, fqname));
    }
  }

  Dst_Hist(char* name):
    ArrayStateVariable<ExtendedPlace<short> >(name, "", Short ,6) {
    char varname[12];
    for (int i=0;i<6;i++) {
      sprintf(varname,"%d",i);
      fields.push_back(new ExtendedPlace<short>(varname, name));
    }
  }

  Dst_Hist(char* name, char* fullname, short & initialValue):
    ArrayStateVariable<ExtendedPlace<short> >(name, fullname, Short ,6) {
    char varname[12];
    char fqname[strlen(fullname) + strlen(name) + 2];
    sprintf(fqname, "%s%s", fullname, name);    for (int i=0;i<6;i++) {
      sprintf(varname,"%d",i);
      fields.push_back(new ExtendedPlace<short>(varname, fqname, initialValue));
    }
  }

  Dst_Hist(char* name, short & initialValue):
    ArrayStateVariable<ExtendedPlace<short> >(name, "", Short ,6) {
    char varname[12];
    for (int i=0;i<6;i++) {
      sprintf(varname,"%d",i);
      fields.push_back(new ExtendedPlace<short>(varname, name, initialValue));
    }
  }

  Dst_Hist(char* name, char* fullname, short * initialValue):
    ArrayStateVariable<ExtendedPlace<short> >(name, fullname, Short ,6) {
    char varname[12];
    char fqname[strlen(fullname) + strlen(name) + 2];
    sprintf(fqname, "%s%s", fullname, name);    for (int i=0;i<6;i++) {
      sprintf(varname,"%d",i);
      fields.push_back(new ExtendedPlace<short>(varname, fqname, initialValue[i]));
    }
  }

  Dst_Hist(char* name, short * initialValue):
    ArrayStateVariable<ExtendedPlace<short> >(name, "", Short ,6) {
    char varname[12];
    for (int i=0;i<6;i++) {
      sprintf(varname,"%d",i);
      fields.push_back(new ExtendedPlace<short>(varname, name, initialValue[i]));
    }
  }
  ~Dst_Hist() {
    for (fieldIterator i=fields.begin();i!=fields.end();++i)
      delete (*i);
  }
};
#endif

/*********************************************************************
               DetectorSAN Submodel Definition                   
*********************************************************************/

class DetectorSAN:public SANModel{
public:

class PacketActivity:public Activity {
public:

  ExtendedPlace<short>* Source;
  ExtendedPlace<short>* Destination;
  Src_Hist* Src1_Hist;
  Src_Hist* Src2_Hist;
  Dst_Hist* Dst1_Hist;
  Dst_Hist* Dst2_Hist;
  Place* Good;
  short* Good_Mobius_Mark;
  Place* Bad;
  short* Bad_Mobius_Mark;
  Place* Departure;
  short* Departure_Mobius_Mark;
  Dst_Hist* Dst3_Hist;
  Src_Hist* Src3_Hist;
  Src_Hist* Src4_Hist;
  Src_Hist* Src5_Hist;
  Src_Hist* Src6_Hist;

  double* TheDistributionParameters;
  PacketActivity();
  double Rate(){return 0;}
  bool Enabled();
  void LinkVariables();
  double Weight();
  bool ReactivationPredicate();
  bool ReactivationFunction();
  double SampleDistribution();
  double* ReturnDistributionParameters();
  int Rank();
  BaseActionClass* Fire();
}; // PacketActivityActivity

  //List of user-specified place names
  Place* Departure;
  Place* Good;
  Place* Bad;
  ExtendedPlace<short>* Source;
  ExtendedPlace<short>* Destination;
  Src_Hist* Src1_Hist;
  Src_Hist* Src2_Hist;
  Dst_Hist* Dst1_Hist;
  Dst_Hist* Dst2_Hist;
  Src_Hist* Src3_Hist;
  Src_Hist* Src4_Hist;
  Dst_Hist* Dst3_Hist;
  Src_Hist* Src5_Hist;
  Src_Hist* Src6_Hist;

  // Create instances of all actvities
  PacketActivity Packet;
  //Create instances of all groups 
  PreselectGroup ImmediateGroup;
  PostselectGroup PacketGroup;

  DetectorSAN();
  ~DetectorSAN();
  void CustomInitialization();

  void assignPlacesToActivitiesInst();
  void assignPlacesToActivitiesTimed();
}; // end DetectorSAN

#endif // DetectorSAN_H_
