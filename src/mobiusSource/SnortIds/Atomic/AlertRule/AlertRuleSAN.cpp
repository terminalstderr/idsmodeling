

// This C++ file was created by SanEditor

#include "Atomic/AlertRule/AlertRuleSAN.h"

#include <stdlib.h>
#include <iostream>

#include <math.h>


/*****************************************************************
                         AlertRuleSAN Constructor             
******************************************************************/


AlertRuleSAN::AlertRuleSAN(){


  ProcessPacketGroup.initialize(4, "ProcessPacketGroup");
  ProcessPacketGroup.appendGroup((BaseGroupClass*) &ProcessPacket_case1);
  ProcessPacketGroup.appendGroup((BaseGroupClass*) &ProcessPacket_case2);
  ProcessPacketGroup.appendGroup((BaseGroupClass*) &ProcessPacket_case3);
  ProcessPacketGroup.appendGroup((BaseGroupClass*) &ProcessPacket_case4);

  Activity* InitialActionList[4]={
    &ProcessPacket_case1, //0
    &ProcessPacket_case2, //1
    &ProcessPacket_case3, //2
    &ProcessPacket_case4  // 3
  };

  BaseGroupClass* InitialGroupList[1]={
    (BaseGroupClass*) &(ProcessPacketGroup)
  };

  source = new Place("source" ,0);
  SinkPositive = new Place("SinkPositive" ,0);
  SinkNegative = new Place("SinkNegative" ,0);
  BaseStateVariableClass* InitialPlaces[3]={
    source,  // 0
    SinkPositive,  // 1
    SinkNegative   // 2
  };
  BaseStateVariableClass* InitialROPlaces[0]={
  };
  initializeSANModelNow("AlertRule", 3, InitialPlaces, 
                        0, InitialROPlaces, 
                        4, InitialActionList, 1, InitialGroupList);


  assignPlacesToActivitiesInst();
  assignPlacesToActivitiesTimed();

  int AffectArcs[4][2]={ 
    {0,0}, {0,1}, {0,2}, {0,3}
  };
  for(int n=0;n<4;n++) {
    AddAffectArc(InitialPlaces[AffectArcs[n][0]],
                 InitialActionList[AffectArcs[n][1]]);
  }
  int EnableArcs[4][2]={ 
    {0,0}, {0,1}, {0,2}, {0,3}
  };
  for(int n=0;n<4;n++) {
    AddEnableArc(InitialPlaces[EnableArcs[n][0]],
                 InitialActionList[EnableArcs[n][1]]);
  }

  for(int n=0;n<4;n++) {
    InitialActionList[n]->LinkVariables();
  }
  CustomInitialization();

}

void AlertRuleSAN::CustomInitialization() {

}
AlertRuleSAN::~AlertRuleSAN(){
  for (int i = 0; i < NumStateVariables-NumReadOnlyPlaces; i++)
    delete LocalStateVariables[i];
};

void AlertRuleSAN::assignPlacesToActivitiesInst(){
}
void AlertRuleSAN::assignPlacesToActivitiesTimed(){
  ProcessPacket_case1.source = (Place*) LocalStateVariables[0];
  ProcessPacket_case2.source = (Place*) LocalStateVariables[0];
  ProcessPacket_case3.source = (Place*) LocalStateVariables[0];
  ProcessPacket_case4.source = (Place*) LocalStateVariables[0];
}
/*****************************************************************/
/*                  Activity Method Definitions                  */
/*****************************************************************/

/*======================ProcessPacketActivity_case1========================*/

AlertRuleSAN::ProcessPacketActivity_case1::ProcessPacketActivity_case1(){
  TheDistributionParameters = new double[1];
  ActivityInitialize("ProcessPacket_case1",0,Exponential, RaceEnabled, 1,1, false);
}

AlertRuleSAN::ProcessPacketActivity_case1::~ProcessPacketActivity_case1(){
  delete[] TheDistributionParameters;
}

void AlertRuleSAN::ProcessPacketActivity_case1::LinkVariables(){
  source->Register(&source_Mobius_Mark);
}

bool AlertRuleSAN::ProcessPacketActivity_case1::Enabled(){
  OldEnabled=NewEnabled;
  NewEnabled=(((*(source_Mobius_Mark)) >=1));
  return NewEnabled;
}

double AlertRuleSAN::ProcessPacketActivity_case1::Rate(){
  return AlertRule_rate;
  return 1.0;  // default rate if none is specified
}

double AlertRuleSAN::ProcessPacketActivity_case1::Weight(){ 
  return // True Negative
AlertRule_tn;
}

bool AlertRuleSAN::ProcessPacketActivity_case1::ReactivationPredicate(){ 
  return false;
}

bool AlertRuleSAN::ProcessPacketActivity_case1::ReactivationFunction(){ 
  return false;
}

double AlertRuleSAN::ProcessPacketActivity_case1::SampleDistribution(){
  return TheDistribution->Exponential(AlertRule_rate);
}

double* AlertRuleSAN::ProcessPacketActivity_case1::ReturnDistributionParameters(){
  TheDistributionParameters[0] = Rate();
  return TheDistributionParameters;
}

int AlertRuleSAN::ProcessPacketActivity_case1::Rank(){
  return 1;
}

BaseActionClass* AlertRuleSAN::ProcessPacketActivity_case1::Fire(){
  (*(source_Mobius_Mark))--;
  AlertRule_tn++;
  return this;
}

/*======================ProcessPacketActivity_case2========================*/

AlertRuleSAN::ProcessPacketActivity_case2::ProcessPacketActivity_case2(){
  TheDistributionParameters = new double[1];
  ActivityInitialize("ProcessPacket_case2",0,Exponential, RaceEnabled, 1,1, false);
}

AlertRuleSAN::ProcessPacketActivity_case2::~ProcessPacketActivity_case2(){
  delete[] TheDistributionParameters;
}

void AlertRuleSAN::ProcessPacketActivity_case2::LinkVariables(){
  source->Register(&source_Mobius_Mark);
}

bool AlertRuleSAN::ProcessPacketActivity_case2::Enabled(){
  OldEnabled=NewEnabled;
  NewEnabled=(((*(source_Mobius_Mark)) >=1));
  return NewEnabled;
}

double AlertRuleSAN::ProcessPacketActivity_case2::Rate(){
  return AlertRule_rate;
  return 1.0;  // default rate if none is specified
}

double AlertRuleSAN::ProcessPacketActivity_case2::Weight(){ 
  return // False Negative
AlertRule_fn;
}

bool AlertRuleSAN::ProcessPacketActivity_case2::ReactivationPredicate(){ 
  return false;
}

bool AlertRuleSAN::ProcessPacketActivity_case2::ReactivationFunction(){ 
  return false;
}

double AlertRuleSAN::ProcessPacketActivity_case2::SampleDistribution(){
  return TheDistribution->Exponential(AlertRule_rate);
}

double* AlertRuleSAN::ProcessPacketActivity_case2::ReturnDistributionParameters(){
  TheDistributionParameters[0] = Rate();
  return TheDistributionParameters;
}

int AlertRuleSAN::ProcessPacketActivity_case2::Rank(){
  return 1;
}

BaseActionClass* AlertRuleSAN::ProcessPacketActivity_case2::Fire(){
  (*(source_Mobius_Mark))--;
  AlertRule_fn++;
  return this;
}

/*======================ProcessPacketActivity_case3========================*/

AlertRuleSAN::ProcessPacketActivity_case3::ProcessPacketActivity_case3(){
  TheDistributionParameters = new double[1];
  ActivityInitialize("ProcessPacket_case3",0,Exponential, RaceEnabled, 1,1, false);
}

AlertRuleSAN::ProcessPacketActivity_case3::~ProcessPacketActivity_case3(){
  delete[] TheDistributionParameters;
}

void AlertRuleSAN::ProcessPacketActivity_case3::LinkVariables(){
  source->Register(&source_Mobius_Mark);
}

bool AlertRuleSAN::ProcessPacketActivity_case3::Enabled(){
  OldEnabled=NewEnabled;
  NewEnabled=(((*(source_Mobius_Mark)) >=1));
  return NewEnabled;
}

double AlertRuleSAN::ProcessPacketActivity_case3::Rate(){
  return AlertRule_rate;
  return 1.0;  // default rate if none is specified
}

double AlertRuleSAN::ProcessPacketActivity_case3::Weight(){ 
  return // True Positive
AlertRule_tp;
}

bool AlertRuleSAN::ProcessPacketActivity_case3::ReactivationPredicate(){ 
  return false;
}

bool AlertRuleSAN::ProcessPacketActivity_case3::ReactivationFunction(){ 
  return false;
}

double AlertRuleSAN::ProcessPacketActivity_case3::SampleDistribution(){
  return TheDistribution->Exponential(AlertRule_rate);
}

double* AlertRuleSAN::ProcessPacketActivity_case3::ReturnDistributionParameters(){
  TheDistributionParameters[0] = Rate();
  return TheDistributionParameters;
}

int AlertRuleSAN::ProcessPacketActivity_case3::Rank(){
  return 1;
}

BaseActionClass* AlertRuleSAN::ProcessPacketActivity_case3::Fire(){
  (*(source_Mobius_Mark))--;
  AlertRule_tp++;
  return this;
}

/*======================ProcessPacketActivity_case4========================*/

AlertRuleSAN::ProcessPacketActivity_case4::ProcessPacketActivity_case4(){
  TheDistributionParameters = new double[1];
  ActivityInitialize("ProcessPacket_case4",0,Exponential, RaceEnabled, 1,1, false);
}

AlertRuleSAN::ProcessPacketActivity_case4::~ProcessPacketActivity_case4(){
  delete[] TheDistributionParameters;
}

void AlertRuleSAN::ProcessPacketActivity_case4::LinkVariables(){
  source->Register(&source_Mobius_Mark);
}

bool AlertRuleSAN::ProcessPacketActivity_case4::Enabled(){
  OldEnabled=NewEnabled;
  NewEnabled=(((*(source_Mobius_Mark)) >=1));
  return NewEnabled;
}

double AlertRuleSAN::ProcessPacketActivity_case4::Rate(){
  return AlertRule_rate;
  return 1.0;  // default rate if none is specified
}

double AlertRuleSAN::ProcessPacketActivity_case4::Weight(){ 
  return // False Positive
AlertRule_fp;
}

bool AlertRuleSAN::ProcessPacketActivity_case4::ReactivationPredicate(){ 
  return false;
}

bool AlertRuleSAN::ProcessPacketActivity_case4::ReactivationFunction(){ 
  return false;
}

double AlertRuleSAN::ProcessPacketActivity_case4::SampleDistribution(){
  return TheDistribution->Exponential(AlertRule_rate);
}

double* AlertRuleSAN::ProcessPacketActivity_case4::ReturnDistributionParameters(){
  TheDistributionParameters[0] = Rate();
  return TheDistributionParameters;
}

int AlertRuleSAN::ProcessPacketActivity_case4::Rank(){
  return 1;
}

BaseActionClass* AlertRuleSAN::ProcessPacketActivity_case4::Fire(){
  (*(source_Mobius_Mark))--;
  AlertRule_fp++;
  return this;
}

