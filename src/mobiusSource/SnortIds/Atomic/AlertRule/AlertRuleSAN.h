#ifndef AlertRuleSAN_H_
#define AlertRuleSAN_H_

#include "Cpp/BaseClasses/EmptyGroup.h"
#include "Cpp/BaseClasses/PreselectGroup.h"
#include "Cpp/BaseClasses/PostselectGroup.h"
#include "Cpp/BaseClasses/state/StructStateVariable.h"
#include "Cpp/BaseClasses/state/ArrayStateVariable.h"
#include "Cpp/BaseClasses/SAN/SANModel.h" 
#include "Cpp/BaseClasses/SAN/Place.h"
#include "Cpp/BaseClasses/SAN/ExtendedPlace.h"
extern UserDistributions* TheDistribution;

void MemoryError();


/*********************************************************************
               AlertRuleSAN Submodel Definition                   
*********************************************************************/

class AlertRuleSAN:public SANModel{
public:

class ProcessPacketActivity_case1:public Activity {
public:

  Place* source;
  short* source_Mobius_Mark;

  double* TheDistributionParameters;
  ProcessPacketActivity_case1();
  ~ProcessPacketActivity_case1();
  bool Enabled();
  void LinkVariables();
  double Weight();
  bool ReactivationPredicate();
  bool ReactivationFunction();
  double SampleDistribution();
  double* ReturnDistributionParameters();
  int Rank();
  BaseActionClass* Fire();
  double Rate();
}; // ProcessPacketActivity_case1Activity

class ProcessPacketActivity_case2:public Activity {
public:

  Place* source;
  short* source_Mobius_Mark;

  double* TheDistributionParameters;
  ProcessPacketActivity_case2();
  ~ProcessPacketActivity_case2();
  bool Enabled();
  void LinkVariables();
  double Weight();
  bool ReactivationPredicate();
  bool ReactivationFunction();
  double SampleDistribution();
  double* ReturnDistributionParameters();
  int Rank();
  BaseActionClass* Fire();
  double Rate();
}; // ProcessPacketActivity_case2Activity

class ProcessPacketActivity_case3:public Activity {
public:

  Place* source;
  short* source_Mobius_Mark;

  double* TheDistributionParameters;
  ProcessPacketActivity_case3();
  ~ProcessPacketActivity_case3();
  bool Enabled();
  void LinkVariables();
  double Weight();
  bool ReactivationPredicate();
  bool ReactivationFunction();
  double SampleDistribution();
  double* ReturnDistributionParameters();
  int Rank();
  BaseActionClass* Fire();
  double Rate();
}; // ProcessPacketActivity_case3Activity

class ProcessPacketActivity_case4:public Activity {
public:

  Place* source;
  short* source_Mobius_Mark;

  double* TheDistributionParameters;
  ProcessPacketActivity_case4();
  ~ProcessPacketActivity_case4();
  bool Enabled();
  void LinkVariables();
  double Weight();
  bool ReactivationPredicate();
  bool ReactivationFunction();
  double SampleDistribution();
  double* ReturnDistributionParameters();
  int Rank();
  BaseActionClass* Fire();
  double Rate();
}; // ProcessPacketActivity_case4Activity

  //List of user-specified place names
  Place* source;
  Place* SinkPositive;
  Place* SinkNegative;

  // Create instances of all actvities
  ProcessPacketActivity_case1 ProcessPacket_case1;
  ProcessPacketActivity_case2 ProcessPacket_case2;
  ProcessPacketActivity_case3 ProcessPacket_case3;
  ProcessPacketActivity_case4 ProcessPacket_case4;
  //Create instances of all groups 
  EmptyGroup ImmediateGroup;
  PostselectGroup ProcessPacketGroup;
  AlertRuleSAN();
  ~AlertRuleSAN();
  void CustomInitialization();

  void assignPlacesToActivitiesInst();
  void assignPlacesToActivitiesTimed();
}; // end AlertRuleSAN

#endif // AlertRuleSAN_H_
